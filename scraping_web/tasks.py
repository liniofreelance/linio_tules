from celery import Celery
#import general_tasks

from pprint import pprint

from isbn_to_amazon import getDataFromAmazonUsingISBN
from isbn_to_sbs import getDataFromSBSUsingISBN
from isbn_to_casadellibro import getDataFromCasaDelLibroUsingISBN

from pymongo import MongoClient
from bson.objectid import ObjectId

client = MongoClient()
db = client.lineo
#jobscoll = db.jobs
#itemcoll = db.items

jobtypeconfs = {
    "amazon" : {
        "jobscoll" : db.jobs,
        "itemcoll" : db.items,
        "objdataattr" : "amazondata",
        "getdatamethod" : getDataFromAmazonUsingISBN
    },
    "sbs" : {
        "jobscoll" : db.sbs_jobs,
        "itemcoll" : db.sbs_items,
        "objdataattr" : "sbsdata",
        "getdatamethod" : getDataFromSBSUsingISBN
    },
    "casadellibro" : {
        "jobscoll" : db.casadellibro_jobs,
        "itemcoll" : db.casadellibro_items,
        "objdataattr" : "casadellibrodata",
        "getdatamethod" : getDataFromCasaDelLibroUsingISBN
    },
}

app = Celery('tasks', broker='amqp://guest@localhost//')

def genericScrapeData(isbn, jobtype) :
    jobscoll = jobtypeconfs[jobtype]["jobscoll"]
    itemcoll = jobtypeconfs[jobtype]["itemcoll"]
    objdataattr = jobtypeconfs[jobtype]["objdataattr"]
    getdatamethod = jobtypeconfs[jobtype]["getdatamethod"]
    item = itemcoll.find_one({"isbn":isbn})
    
    if item == None :
        print "ORPHAN ISBN: %s" % isbn
        return 
    
    if item["status"] == "aborted" :
        print "ABORTED ISBN: %s" % isbn
        checkIfJobIsDone.delay(isbn, jobtype)
        return
    
    try :
        data = getdatamethod(isbn)
        print data
        if data == None :
            raise Exception
        item["status"] = "done"
        item[objdataattr] = data
        #pprint(item)
        itemcoll.update({"_id":item["_id"]}, item, upsert=False)
    except :
        item["status"] = "failed"
        itemcoll.update({"_id":item["_id"]}, item, upsert=False)
    
    checkIfJobIsDone.delay(isbn, jobtype)

@app.task(name='batch_scraping_task.AmazonScrapeData')
def amazonScrapeData(isbn):
    return genericScrapeData(isbn, "amazon")

@app.task(name='batch_scraping_task.SBSScrapeData')
def sbsScrapeData(isbn):
    return genericScrapeData(isbn, "sbs")

@app.task(name='batch_scraping_task.CasaDelLibroScrapeData')
def casadellibroScrapeData(isbn):
    return genericScrapeData(isbn, "casadellibro")

jobtypeconfs["amazon"]["taskmethod"] = amazonScrapeData
jobtypeconfs["sbs"]["taskmethod"] = sbsScrapeData
jobtypeconfs["casadellibro"]["taskmethod"] = casadellibroScrapeData

@app.task
def checkIfJobIsDone(isbn, jobtype):
    jobscoll = jobtypeconfs[jobtype]["jobscoll"]
    itemcoll = jobtypeconfs[jobtype]["itemcoll"]
    for job in jobscoll.find({"isbnlist": isbn}) :
        items = itemcoll.find({"isbn":{"$in": job["isbnlist"]}})
        res = [item["status"] for item in items]
        if ("new" in res) or ("runnning" in res) :
            continue
        if "failed" in res :
            job["status"] = "done_w_errors"
        if "aborted" in res :
            job["status"] = "aborted"
        if all([r=="done" for r in res]) :
            job["status"] = "done"
        jobscoll.update({"_id":job["_id"]}, job, upsert=False)


app.control.rate_limit('batch_scraping_task.AmazonScrapeData', '1/s')

