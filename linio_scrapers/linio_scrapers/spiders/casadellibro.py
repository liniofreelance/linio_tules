# -*- coding: utf8 -*-

import scrapy
from linio_scrapers.items import GenericListingItem, GenericItem
import requests

import random

from pprint import pprint
import json
import re

"""
scrapy crawl casadellibro -a target=dumpall -o  /tmp/casadellibro_dumpall.json
scrapy crawl casadellibro -a target=singlebook
scrapy crawl casadellibro -a target=singlebook -a targetinp="http://www.casadellibro.com/libro-grecia/9788494261244/2557288"
scrapy crawl casadellibro -a target=singlebook -a targetinp="http://www.casadellibro.com/libro-sacedon-un-siglo-de-imagenes-612-fotografias-que-marcan-la-vida-y-el-discurrir-de-sacedon-durante-el-siglo-xx/9788496236431/1052684"
"""

def GetFirst(l,v=None) :
    if len(l)>=1 :
        v = l[0]
    if type(v) == str :
        v = v.strip()
    return v

def stripJoin(l,s=',') :
    return s.join(map(lambda e:e.strip(), l))

def normalizeFields (datatable, equivs) :
    itemdata = {}
    miscdata = {}
    for (k,v) in datatable :
        if type(v) == list :
            v = stripJoin(v)
        if equivs.has_key(k) :
            itemdata[equivs[k]] = v
        else :
            miscdata[k] = v
    #itemdata["miscdata"] = json.dumps(miscdata)
    return itemdata


class CasadellibroSpider(scrapy.Spider):
    max_concurrent_requests = 5
    #download_delay = random.uniform(0.1, 0.2) 
    download_delay = 0
    
    name = "casadellibro"
    baseurl = "http://www.casadellibro.com/"
    allowed_domains = ["www.casadellibro.com"]
    start_urls = [
        #"http://www.dmoz.org/Computers/Programming/Languages/Python/Books/",
        #"http://www.dmoz.org/Computers/Programming/Languages/Python/Resources/"
    ]

    target = None
    followbooks = False

    def __init__(self, target='dumpall', followbooks="yes" ,targetinp=None):
        self.target = target
        if self.target == "dumpall" :
            self.start_urls = ["http://www.casadellibro.com/busqueda-libros"]
            self.parse = self.parselisting
        #if self.target == "novedades" :
        #    self.start_urls = ["http://www.cuspide.com/novedades/"]
        #    self.parse = self.parselistingfollowlinks
        if self.target == "singlebook" :
            self.start_urls = [targetinp if targetinp != None else "http://www.casadellibro.com/libro-cuadernos-de-hiroshima/9788433963291/1890896"]
            self.parse = self.parsebookpage
        if self.target == "bookbatch" :
            if targetinp == None :
                print "Define targets first!"
            self.start_urls = [fn.strip() for fn in open(targetinp).readlines()]
            self.parse = self.parsebookpage
    
    def parse(self, response):
        pass
    
    #def parselistingfollowlinks (self, response) :
    #    for url in response.css("ul#vertnav").xpath(".//a/@href").extract() :
    #        yield scrapy.Request(url, callback=self.parselisting)
        
    def parselisting (self, response) :
        #for url in response.css("div.mod-list-item > div.txt > a").xpath(".//@href").extract() :
        #    yield scrapy.Request(self.baseurl+url, callback=self.parsebookpage)
        
        for entry in response.css("div.mod-list-item") :
            itemdata = {}
            bookurl = GetFirst(entry.css("a.title-link::attr(href)").extract())
            itemdata["url"] = self.baseurl + bookurl
            itemdata["isbn"] = bookurl.split("/")[-2]
            itemdata["title"] = GetFirst(entry.css("a.title-link::text").extract())
            #itemdata["author"] = GetFirst(entry.css("a.author-link::text").extract())
            itemdata["author"] = stripJoin(entry.css("div.mod-libros-author").xpath(".//text()").extract(),"")
            itemdata["publisher"] = GetFirst(entry.css("div.mod-libros-editorial::text").extract(),"").strip()
            itemdata["year"] = GetFirst(entry.css("div.mod-libros-editorial > span::text").extract(),"").strip()
            itemdata["prices"] = GetFirst(entry.css("p.currentPrice::text").extract())
            #itemdata["cat"] = img-shadow
            itemdata["img"] = GetFirst(entry.css("img.img-shadow::attr(src)").extract())
            #itemdata["desc"] = GetFirst(entry.css("p.smaller.pb15::text").extract(), "").strip() + " " + GetFirst(entry.css("span#sinopsis0::text").extract(), "").strip()
            itemdata["desc"] = " ".join([GetFirst(entry.css("p.smaller.pb15::text").extract(), "").strip(), GetFirst(entry.xpath(".//span[contains(@id,'sinopsis')]/text()").extract(), "").strip()])
            itemdata["miscdata"] = {}
            itemdata["miscdata"]["num_guardado"] = GetFirst(entry.css(".event-layer-guardadopor::text").extract(),"").replace("personas","").strip()
            #print json.dumps(itemdata, indent=4)
            itemdata["miscdata"] = json.dumps(itemdata["miscdata"])
            yield GenericItem(**itemdata)
        
        for url in response.css("ul.paginationOp > li").xpath(".//a/@href").extract() :
            url = re.sub("&udm=[0-9]+", "", url)
            url = re.sub("&nivel=[0-9]+", "", url)
            url = re.sub("&itemxpagina=[0-9]+", "", url)
            yield scrapy.Request(self.baseurl+url, callback=self.parselisting)
    
    def parsebookpage (self, response) :
        datatable = []
        for r in response.css("ul.list07 > li") :
            k = stripJoin(r.xpath("./span/text()").extract())
            v = stripJoin(r.xpath("./text()").extract())
            datatable += [(k,v)]
        
        equivs = {
            u'Lengua:': 'lang',
            u'Nº de páginas:': 'pages',
            u'ISBN:': 'isbn',
            u'Año edición:': 'year',
            u'Encuadernación:': 'format',
        }
        
        itemdata = normalizeFields(datatable, equivs)
        itemdata["url"] = response.url
        #itemdata["img"] = response.css(".product-img-box").xpath(".//a/@href").extract()
        
        itemdata["title"] = response.css("h1.book-header-2-title::text").extract()
        itemdata["author"] =  response.css("div.book-header").css("a.book-header-2-subtitle-author::text").extract()
        itemdata["publisher"] = response.css("span.book-header-2-subtitle-publisher::text").extract()
        #itemdata["prices"] = response.css("p.currentPrice::text").extract()[0]
        itemdata["cat"] = stripJoin(response.css("ul.bread > li.expmat > a::text").extract())
        #itemdata["desc"] = response.css("span#sinopsis::text").extract()
        itemdata["img"] = response.css("img#imgPrincipal::attr(src)").extract()
        #itemdata["url"] = response.url
        
        itemdata["desc"] = response.xpath("//div[./h3/text()='Resumen del libro']").css("p.smaller::text").extract()
        
        """
        El resultado de "modales/ofertaProducto?idproducto" se usa aca:
        http://static.casadellibro.com/t1e/js/ofertas.js
        """
        extradataurl_tpl = "http://www.casadellibro.com/modales/ofertaProducto?idproducto=%s"
        try :
            extradata = requests.get(extradataurl_tpl % response.url.split("/")[-1]).json()
        except FloatingPointError:
            pass
        
        try :
            itemdata["prices"] = "%s %s" % (extradata["pfinal"], extradata["moneda"])
        except FloatingPointError:
            pass
        
        try :
            itemdata["availavility"] = {
                "1" : "en_stock",
                "2" : "disponible",
                "3" : "ebook",
                "4" : "preventa_fisico",
                "11" : "preventa_ebook",
                "5" : "agotado",
                "6" : "descatalogado",
                "7" : "ebook_gratis",
                "8" : "vendible",
                "9" : "vendible",
                "10" : "vendible",
                "11" : "vendible",
            }.get(extradata["estadodisponibilidad"], "desconocido")
            itemdata["miscdata"]["raw_availavility"] = extradata["estadodisponibilidad"]
        except :
            pass
        
        itemdata["miscdata"] = json.dumps(itemdata.get("miscdata",{}))
        
        for k in itemdata.keys() :
            if type(itemdata[k]) == list :
                itemdata[k] = stripJoin(itemdata[k])
        
        item = GenericItem(**itemdata)
        return item
        
        
