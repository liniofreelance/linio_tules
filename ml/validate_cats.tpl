<!doctype html>
<html lang="en">
<head>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

</head>

<body>

<div class="row">
    <div class="col-md-8">
        <h1>Se encontraron categorias invalidas:</h1>
    </div>
</div>
<div class="row">

<!--div class="col-md-3">
  <h4>Categorias validas:</h4>
  <ul class="list-group">
  {% for c in valid %}
    <li class="list-group-item">{{ c }}</li>
  {% endfor %}
  </ul>
</div-->

<div class="col-md-3">
  <h4>Categorias invalidas:</h4>
  <ul class="list-group">
  {% for c in invalid %}
    <li class="list-group-item">{{ c }}</li>
  {% endfor %}
  </ul>
  <p>(Las categorias de esta lista seran ignoradas)
</div>

<div class="col-md-3">
  <h4>Desea continuar de todas formas?</h4>
  <form action="launch_work" method="POST">
    <div class="form-group">
      <!--label for="exampleInputEmail1">Cantidad maxima de items a descargar:</label>
      <input type="text" class="form-control" id="maxitemsInput" name="maxitems"-->
      <input type="hidden" name="maxitems" value="{{ maxitems }}" />
      {% for catcode in valid %}
        <input type="hidden" name="catcodes" value="{{ catcode }}" />
      {% endfor %}
      <input class="btn btn-success" type='submit' value='Iniciar'>
      <a class="btn btn-danger" href="/">Cancelar</a>
    </div>
  </form>
</div>
</div>

</body>
</html>