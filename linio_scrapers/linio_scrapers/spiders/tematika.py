 # -*- coding: utf-8 -*-
 
import os.path
import HTMLParser
from lxml import html
import json

import requests
from scrapy.http import HtmlResponse

import scrapy
from scrapy.http import Request
from linio_scrapers.items import GenericListingItem, GenericItem, GenericCategoryListingItem
from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst, MapCompose, Join

"""



scrapy crawl tematika -a target=item -a targetinp="http://www.tematika.com/libros/autoayuda--5/superacion_personal--5/camino_mas_facil_para_crecer--593813.htm"
"""

def GetFirst(l,v=None) :
    if len(l)>=1 :
        v = l[0]
    if type(v) == str :
        v = v.strip()
    return v

class TematikaSpider(scrapy.Spider):
    name = "tematika"
    baseurl = "http://www.tematika.com/libros/"
    allowed_domains = ["www.tematika.com"]
    #start_urls = [
    #    "http://http://www.tematika.com/"
    #]
    
    target = None
    
    def __init__(self, target="dumpall", followbooks="yes", targetinp=None):
        #import time; time.sleep(99999)
        self.target = target
        self.start_urls = [targetinp]
        if self.target in ["dumpall", "dumpcategories"] :
            self.setupDumpAll()
        if self.target in ["dumplisting", "dumplistingbooks", "dumplistingcontents"] :
            self.parse = self.parseListing
        if self.target == "item" :
            self.parse = self.parseItem
        if self.target == "novelties" :
            self.setupDumpNovelties()
        if self.target == "bestsellers" :
            self.setupDumpBestsellers()
    
    def getBestsellersAndNovelties(self):
        def parseSection (raw, startmark) :
            html = raw[raw.index(startmark):]
            html = html[len(startmark):]
            html = html[:html.index("'")-1]
            response = HtmlResponse (url=url, body=html)
            return response.css("td.product-entry").xpath("./a/@href").extract()
        
        url = "http://www.tematika.com/showcase/libros.js"
        raw = requests.get(url).content
        return {
                "bestsellers" : parseSection(raw, "dataShowCase['Showcase'] = '"),
                "novelties" : parseSection(raw, "dataShowCase['Classics'] = '")     # Si, esto esta bien
            }
    
    def setupDumpAll (self) :
        self.start_urls = ["http://www.tematika.com/libros"]
        self.parse = self.parseCategories
        
    def setupDumpNovelties(self) :
        self.start_urls = ["http://www.tematika.com/" + url for url in self.getBestsellersAndNovelties()["novelties"]]
        self.parse = self.parseItem
    
    def setupDumpBestsellers(self) :
        self.start_urls = ["http://www.tematika.com/" + url for url in self.getBestsellersAndNovelties()["bestsellers"]]
        self.parse = self.parseItem
    
    #def parse (self, response) :
    def parseCategories (self, response) :
        fn = os.path.split(__file__)[0] + "/" + "tematika_dataTree"
        raw = open(fn).read()
        h = HTMLParser.HTMLParser()
        rawhtml = h.unescape(raw.decode("utf8"))
        doc = html.fromstring(rawhtml)
        urls = doc.xpath("//a/@href")
        catalogurltpl = "http://www.tematika.com/catalog/%s"
        catalogurls = [catalogurltpl % url.replace(".htm",".js")  for url in urls]
        for url in catalogurls :
            yield Request(url, callback=self.parseListing)
    
    def parseListing (self, response) :
        raw = response.body
        startmark = "'catalog', "
        endmark = ");"
        try :
            catalogjson = raw[raw.index(startmark)+len(startmark):raw.index(endmark)].replace("'",'"').replace("][","],[")
        except :
            return
        
        if len(catalogjson) == 0 : 
            return
        
        rawdata = json.loads(catalogjson)
        catalogdata = []
        for rawitem in rawdata :
            listingitem = dict(zip(["id", "img", "title", "link", "author", "link_author", "unk1", "price", "pubdate"], rawitem))
            catalogdata += [listingitem]
        
        for link in [e["link"] for e in catalogdata] :
            yield Request("http://www.tematika.com/%s.htm" % link, callback=self.parseItem)
         
    
    def parseItem (self, response) :
        """
        l = ItemLoader(item=GenericItem(), response=response)
        l.add_xpath('prices', "//div[@class='datos']//p[@class='precioAhora']//*[@itemprop='price']/text()", TakeFirst())
        l.add_xpath('desc', "//*[@itemprop='description']/p/text()")
        l.add_xpath('title', "//div[@class='datos']/h1[@itemprop='name']/text()")
        l.add_xpath('img', "//*[@itemprop='image']//@src")
        fields = {
            "formato" : "format",
            "titulo" : "title",
            "editorial" : "publisher",
            "autor" : "author",
            "categoria" : "cat",
            "tema" : "subject",
            "idioma" : "lang",
            "numero paginas" : "pages",
            "ano" : "year",
            "isbn13" : "isbn",
        }
        misc = {}
        for det in response.xpath("//ul[@class='detalleEspecial']//li") :
            k = GetFirst(det.xpath("./@class").extract(), "").strip()
            v = ' '.join(det.xpath(".//strong//text()").extract()).strip()
            if fields.has_key(k) :
                l.add_value(fields.get(k,k), v)
            else :
                misc[k] = v
        l.add_value("miscdata", json.dumps(misc))
        return l.load_item()
        """
        datoslibro = response.xpath("//div[@class='detalleInfoPrinc']")
        item = {
            "title" : GetFirst(datoslibro.xpath(".//*[@class='dInfoTit']/h1//text()").extract()),
            "img" : GetFirst(datoslibro.xpath(".//*[@class='detalleImagen']//@src").extract()),
            "author" : GetFirst(datoslibro.xpath(".//*[@class='dInfoAutor']/h2/a//text()").extract()),
            "price" : datoslibro.xpath("//input[@id='precio']/@value").extract(),
            "desc" : GetFirst(datoslibro.xpath("//*[@class='dResena']/text()").extract()),
        }
        misc = datoslibro.xpath(".//*[@class='dInfoPrincipal']/span//text()").extract()
        miscfields = [
            ("I.S.B.N : ", "isbn"),
            ("Disponibilidad: ", "availavility"),
            ("Páginas:", "pages"),
        ]
        k = None
        misciter = iter(misc)
        for e in misciter :
            if 'Editorial: ' in e :
                k = "publisher"
                continue
            
            if u'Clasificaci\xf3n:' in e : 
                cat = []
                e = misciter.next()
                while not ":" in e :
                    if e != u'\xa0\xbb\xa0' :
                        cat += [e]
                    e = misciter.next()
                item["cat"] = ' > '.join(cat)
            for (mark,fielname) in miscfields :
                if mark.decode("utf8") in e :
                    #item[fielname] = e.replace(mark.decode("utf8"), "")
                    item[fielname] = e[len(mark):]
                    k = None
            #print e
            if "Publicación:" in e.encode("utf8") :
                #print e
                vs = map(lambda s: s.strip(), e.split("|"))
                for v in vs :
                    if r"Publicaci\xf3n:" in v :
                        item["pubdate"] = v[len(r"Publicaci\xf3n:"):]
                    if r"Idioma:" in v :
                        item["lang"] = v[len(r"Idioma:"):]
            if k != None :  
                item[k] = e
                k = None
        
        miscdata = {}
        newGenericItem = GenericItem()
        for (k,v) in item.items() :
            if k in newGenericItem.fields.keys() : 
                newGenericItem[k] =  v
            else :
                miscdata[k] = v
        try :
            newGenericItem.miscdata = json.dumps(miscdata)
        except :
            pass
        return newGenericItem



