
import scrapy
import json
from scrapy.http import Request
from linio_scrapers.items import GenericListingItem, GenericItem, GenericCategoryListingItem
from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst, MapCompose, Join

"""
scrapy crawl buscalibre -a target=category -a targetinp="http://www.buscalibre.cl/libros/categoria-arquitectura-diseno-y-tendencias/20"

scrapy crawl buscalibre -a target=category -a targetinp="http://www.buscalibre.cl/libros/categoria-arquitectura-diseno-y-tendencias/20"

scrapy crawl buscalibre -a target=dumpcategories

scrapy crawl buscalibre -a target=dumplisting -a targetinp="http://www.buscalibre.cl/libros/categoria-religion/31"

scrapy crawl buscalibre -a target=dumplistingcontents -a targetinp="http://www.buscalibre.cl/libros/categoria-religion/31"
"""

def GetFirst(l,v=None) :
    if len(l)>=1 :
        v = l[0]
    if type(v) == str :
        v = v.strip()
    return v

class BuscalibreSpider(scrapy.Spider):
    name = "buscalibre"
    baseurl = "http://www.buscalibre.cl/"
    allowed_domains = ["www.buscalibre.cl"]
    #start_urls = [
    #    "http://www.buscalibre.cl/libros"
    #]
    
    target = None
    
    def __init__(self, target="dumpall", followbooks="yes", targetinp=None):
        #import time; time.sleep(99999)
        self.target = target
        self.start_urls = [targetinp]
        if self.target in ["dumpall", "dumpcategories"] :
            self.start_urls = ["http://www.buscalibre.cl/libros"]
            self.parse = self.parseCategories
        if self.target in ["dumplisting", "dumplistingbooks", "dumplistingcontents"] :
            self.parse = self.parseListing
        if self.target == "item" :
            self.parse = self.parseItem
    
    #def parse (self, response) :
    def parseCategories (self, response) :
        titles = response.xpath("//a[@itemprop='WPSideBar']/@title").extract()
        urls = response.xpath("//a[@itemprop='WPSideBar']/@href").extract()
        if self.target == 'dumpcategories' :
            for (t,u) in zip(titles, urls) :
                yield GenericCategoryListingItem({"title":t, "url":u})
        
        if self.target in ["dumpall"] :
            for url in urls :
                yield Request(url, callback=self.parseListing)
    
    def parseListing (self, response) :
        if self.target == "dumplisting" :
            for prod in response.xpath("//div[@class='producto ']") :
                data = {
                    "url" : GetFirst(prod.xpath("./a/@href").extract(),"").strip(),
                    "img" : GetFirst(prod.xpath("./a/div[@class='imagen']/img/@src").extract(),"").strip(),
                    "title" : GetFirst(prod.xpath("./a/div[@class='nombre']/text()").extract(),"").strip(),
                    "author" : GetFirst(prod.xpath("./a/div[@class='autor']/text()").extract(),"").strip(),
                }
                yield GenericListingItem(data)
        
        if self.target in ["dumpall", "dumplistingcontents"] :
            for prod in response.xpath("//div[@class='producto ']") :
                yield Request(GetFirst(prod.xpath("./a/@href").extract(),"").strip(), callback=self.parseItem)
        
        for url in response.xpath("//div[@id='paginacion']//a/@href").extract() :
            yield Request(url.strip(), callback=self.parseListing)
            
    def parseItem (self, response) :
        l = ItemLoader(item=GenericItem(), response=response)
        l.add_xpath('prices', "//div[@class='datos']//p[@class='precioAhora']//*[@itemprop='price']/text()", TakeFirst())
        l.add_xpath('desc', "//*[@itemprop='description']/p/text()")
        l.add_xpath('title', "//div[@class='datos']/h1[@itemprop='name']/text()")
        l.add_xpath('img', "//*[@itemprop='image']//@src")
        fields = {
            "formato" : "format",
            "titulo" : "title",
            "editorial" : "publisher",
            "autor" : "author",
            "categoria" : "cat",
            "tema" : "subject",
            "idioma" : "lang",
            "numero paginas" : "pages",
            "ano" : "year",
            "isbn13" : "isbn",
        }
        misc = {}
        for det in response.xpath("//ul[@class='detalleEspecial']//li") :
            k = GetFirst(det.xpath("./@class").extract(), "").strip()
            v = ' '.join(det.xpath(".//strong//text()").extract()).strip()
            if fields.has_key(k) :
                l.add_value(fields.get(k,k), v)
            else :
                misc[k] = v
        l.add_value("miscdata", json.dumps(misc))
        return l.load_item()
        
        
        
               
        
