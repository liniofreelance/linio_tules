<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    </head>
    <body>
        {%
            set resultText = {
                "cancel" : "fue cancelado.",
                "delete" : "fue borrado.",
                "add" : "fue creado con exito.",
            }[textSelector]
        %}
        <h2>El trabajo {{ job.name }} ({{ job._id }}) {{ resultText }}</h2>
        <a class="btn btn-default" href="/">Volver</a>
    </body>
</html>
