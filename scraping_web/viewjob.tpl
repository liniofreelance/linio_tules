<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="panel panel-default"><div class="panel-body">
        <h1>Estado del trabajo {{ job.name }}({{ job._id }}) :</h1>
        <a class="btn btn-default" href="downloadjobresults?id={{ job._id }}&fmt=xls&jobtype={{ jobtype }}">Descargar resultados actuales</a>
        <a class="btn btn-default" href="viewjobresults?id={{ job._id }}&jobtype={{ jobtype }}">Ver resultados actuales</a>
        <table class="table">
            <thead>
                <th>ISBN</th>
                <th>Estado</th>
            </thead>
            <tbody>
                {% for entry in job.tasks %}
                <tr>
                    <!--td>{{ entry }}</td-->
                    <td>{{ entry.isbn }}</td>
                    <td>{{ entry.status }}</td>
                </tr>
                {% endfor %}
            </tbody>
        </table>
        <a class="btn btn-default" href="/">Volver</a>
        </div></div>
    </body>
</html>
