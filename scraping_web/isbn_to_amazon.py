

#https://www.staff.ncl.ac.uk/d.j.wilkinson/software/isbn.py
import isbnconverter

import json
from pprint import pprint

import requests
from scrapy.http import HtmlResponse

#isbn10 = "978-0-345-50534-7"
#isbn10 = "978-987-61-5014-9"
#isbn10 = "978-0-230-46029-4"

def TakeFirstStrip (l) :
    if len(l) > 0 :
        return l[0].strip()
    else :
        return ""

def JoinStrip (l) :
    return ' '.join(l).strip()

def getDataFromAmazonUsingISBN (isbn10) :
    urltpl = "http://www.amazon.com/-/dp/%s/"
    isbn_formated = isbnconverter.toI10(isbn10)
    if isbn_formated == None :
        return None
    url = urltpl % isbn_formated
    
    headers = {
        'User-Agent': 'Mozilla/5.0 ;Windows NT 6.1; WOW64; rv:36.0; Gecko/20100101 Firefox/36.0',
    }
    
    print url
    
    titledata = []
    imgs = []
    authors = []
    synopsis = ""

    
    resp = requests.get(url, headers=headers)
    response = HtmlResponse(url=url, body=resp.content)

    price = TakeFirstStrip(response.xpath("//span[@id='miniATF_price']/text()").extract())
    
    try :
        raw = response.xpath("//script[contains(text(),'imageGalleryData')]/text()").extract()[0]
        raw = filter(lambda l: "imageGalleryData" in l, raw.split("\n"))[0]
        raw = raw.split("imageGalleryData' : ")[1][:-1]
        imgs = [img["mainUrl"] for img in json.loads(raw)]
    except :
        pass
    
    #synopsis = response.xpath("//noscript/div/text()").extract()
    #synopsis = ' '.join(response.xpath("(//noscript)[2]//text()").extract()).strip()
    synopsis = JoinStrip(response.xpath("//div[@id='bookDescription_feature_div']//noscript//text()").extract())

    if "#DAcrt{display:none;}" in synopsis :
        synopsis = ""
    
    print synopsis
    titledata = response.xpath("//div[@id='booksTitle']/div/h1[@id='title']//text()").extract()
    titledata = filter(lambda e: len(e)>0, [e.strip().replace(u"\u2013 ", "") for e in titledata])
    
    detailsdata = response.xpath("//table[@id='productDetailsTable']//div[@class='content']/ul/li")
    entrydata = {}
    for entry in detailsdata :
        dataname = TakeFirstStrip(entry.xpath("./b/text()").extract()).replace(":","")
        dataval = TakeFirstStrip(entry.xpath("./text()").extract())
        if dataname=="Shipping Weight" :
            dataval = dataval.replace(" (", "")
        entrydata[dataname] = dataval
        #print entrydata
    
    #authors = response.xpath("//a[contains(@class,'contributorNameID')]/text()").extract()
    authors = response.xpath("//span[contains(@class,'author')]/a/text()").extract()
    
    carouselcards = response.xpath("//div[@id='purchase-sims-feature']//li[@role='listitem']")
    sugs = []
    for card in carouselcards :
        entry = {}
        entry["link"] = TakeFirstStrip(card.xpath(".//a[contains(@class,'a-link-normal')]/@href").extract())
        entry["title"] = TakeFirstStrip(card.xpath(".//a[contains(@class,'a-link-normal')]//text()").extract())
        entry["img"] = TakeFirstStrip(card.xpath(".//a[contains(@class,'a-link-normal')]//@src").extract())
        sugs += [entry]
    
    #pprint(sugs)
    
    bookdata = {
    "titledata" : ' - '.join(titledata),
    "price" : price,
    #"imgs" : ' '.join(imgs),
    "authors" : ' - '.join(authors),
    #"entrydata" : entrydata,
    "synopsis" : synopsis,
    "url" : url,
    "suggested" : sugs,
    }
    
    for (i,img) in zip(range(len(imgs)),imgs) :
        bookdata["img%d" % i] = img
    
    bookdata.update(entrydata)
    
    #print imgs, titledata, synopsis, authors, entries
    pprint (bookdata)
    return bookdata

