
from scrapy import Selector
import requests

def getDataFromCasaDelLibroUsingISBN(isbn) :
    baseurl = "http://www.casadellibro.com"
    url_tpl = "%s/buscador/busquedaGenerica?busqueda=%s" 
    url = url_tpl % (baseurl, isbn)
    try :
        content = requests.get(url).content
    except :
        itemdata = {
            "isbn" : isbn,
            "category" : ""
        }
        return itemdata
    sel = Selector(text=content)
    s = sel.css(".title-link::attr(href)")
    if len(s) <= 0 :
        return None
    try :
        if "No hay" in  sel.css("h1.title01-adv-busc::text").extract()[0] or "No encontramos" in  sel.css("h1.title01-adv-busc::text").extract()[0]  :
            itemdata = {
                "isbn" : isbn,
                "category" : ""
            }
            return itemdata
    except :
        pass
    
    url = baseurl + s[0].extract()
    try :
        content = requests.get(url).content
    except :
        itemdata = {
            "isbn" : isbn,
            "category" : ""
        }
        return itemdata
    
    sel = Selector(text=content)
    itemdata = {
        "isbn" : isbn,
        "category" : " > ".join(sel.css("ul.bread > .expmat > a::text").extract())
    }
    return itemdata
