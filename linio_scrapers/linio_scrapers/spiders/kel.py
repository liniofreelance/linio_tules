

import scrapy
from linio_scrapers.items import GenericListingItem, GenericItem

from pprint import pprint
import json

def stripJoin(l) :
    return ','.join(map(lambda e:e.strip(), l))

def normalizeFields (datatable, equivs) :
    itemdata = {}
    miscdata = {}
    for (k,v) in datatable :
        if equivs.has_key(k) :
            itemdata[equivs[k]] = v
        else :
            miscdata[k] = v
    itemdata["miscdata"] = json.dumps(miscdata)
    return itemdata


class KelSpider(scrapy.Spider):
    name = "kel"
    baseurl = "http://www.kelediciones.com/"
    allowed_domains = ["www.kelediciones.com"]
    start_urls = [
        #"http://www.dmoz.org/Computers/Programming/Languages/Python/Books/",
        #"http://www.dmoz.org/Computers/Programming/Languages/Python/Resources/"
    ]

    target = None
    followbooks = False

    def __init__(self, target='dumpall', followbooks="yes" ,targetinp=None):
        self.target = target
        if self.target == "dumpall" :
            self.start_urls = ["http://www.kelediciones.com/"]
            self.parse = self.parselisting
        #if self.target == "novedades" :
        #    self.start_urls = ["http://www.cuspide.com/novedades/"]
        #    self.parse = self.parselistingfollowlinks
        if self.target == "singlebook" :
            #self.start_urls = ["http://www.cuspide.com/9789876372688/Huerta+Y+Cocina/"]
            self.start_urls = [targetinp if targetinp != None else "http://www.kelediciones.com/espanol/indice-general/new-arrivals/5th-wave-the-2-the-infinite-sea-puffin-sep-2014.html"]
            #self.start_urls = ["http://www.kelediciones.com/espanol/indice-general/new-arrivals/5th-wave-the-2-the-infinite-sea-puffin-sep-2014.html"]
            self.parse = self.parsebookpage
        if self.target == "bookbatch" :
            if targetinp == None :
                print "Define targets first!"
            self.start_urls = [fn.strip() for fn in open(targetinp).readlines()]
            self.parse = self.parsebookpage
    
    def parse(self, response):
        pass
    
    #def parselistingfollowlinks (self, response) :
    #    for url in response.css("ul#vertnav").xpath(".//a/@href").extract() :
    #        yield scrapy.Request(url, callback=self.parselisting)
        
    def parselisting (self, response) :
        for url in response.css("ul#vertnav").xpath(".//a/@href").extract() + response.css("div.pages").xpath(".//a/@href").extract() :
            yield scrapy.Request(url, callback=self.parselisting)
        #for url in response.css("div.pages").xpath(".//a/@href").extract() :
        #    yield scrapy.Request(url, callback=self.parselisting)
        
        for url in response.css("h2.product-name").xpath(".//a/@href").extract() :
            yield scrapy.Request(url, callback=self.parsebookpage)
    
    def parsebookpage (self, response) :
        datatable = []
        for tr in response.css(".data-table > tbody > tr") :
            hdr = stripJoin(tr.xpath(".//th//text()").extract())
            val = stripJoin(tr.xpath(".//td//text()").extract())
            datatable += [(hdr,val)]
        
        equivs = {
            u'Nombre': 'title',
            u'Peso': 'weight',
            u'Precio': 'prices',
            u'Edici\xf3n': 'edition',
            u'ISBN': 'isbn',
            u'Autor': 'author',
            u'Editorial': 'publisher',
            u'P\xe1ginas': 'pages',
        }
        itemdata = normalizeFields(datatable, equivs)
        itemdata["url"] = response.url
        itemdata["img"] = response.css(".product-img-box").xpath(".//a/@href").extract()[0]
        itemdata["availavility"] = response.xpath("//p[contains(@class,'availability')]/span/text()").extract()[0]
        itemdata["desc"] = ' '.join(response.css("div.short-description > div").xpath(".//text()").extract())
        itemdata["cat"] = ' > '.join(response.css(".breadcrumbs").xpath(".//a/text()").extract())
        item = GenericItem(**itemdata)
        return item
        
        
