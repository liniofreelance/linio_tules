<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    </head>
    <body>
        <!--div class="panel panel-default"><div class="panel-body"-->
        <h1>Datos extraidos:</h1>
        <table class="table table-striped table-hover table-bordered ">
            <thead>
                {% for hdr in headers %}
                <th>
                {{ hdr }}
                </th>
                {% endfor %}
            </thead>
            <tbody>
                {% for entries in lines %}
                <tr>
                    {% for e in entries %}
                    <td>{{ e }}</td>
                    {% endfor %}
                </tr>
                {% endfor %}
            </tbody>
        </table>
        <a class="btn btn-default" href="/">Volver</a>
        <!--/div></div-->
    </body>
</html>
