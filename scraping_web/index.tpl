<!doctype html>
<html>
<head>
    <title>Herramientas de Scraping</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="panel panel-default"><div class="panel-body">
    <h1>Herramienta para extraer datos de Amazon a partir del ISBN</h1>
    <a class="btn btn-default btn-primary" href='addjob?jobtype=amazon'>Agregar nuevo trabajo</a>
    <br>
    <br>
    <a class="btn btn-default btn-success" href='listjobs?jobtype=amazon'>Ver trabajos</a>
    <br>
    <br>
    <a class="btn btn-default btn-warning" href='purgejobs?type=amazon'>Limpiar trabajos no terminados</a>
    <br>
    <br>
    <form class="form-inline" action="getisbndata">
      <div class="form-group">
        <span>Consultar un solo ISBN:</span>
        <br>
        <input type="text" class="form-control" name="isbn" placeholder="ISBN">
        <input type="hidden" class="form-control" name="jobtype" value="amazon">
        <br>
        <button type="submit" class="btn btn-default">Enviar</button>
      </div>
    </form>
    </div></div>

    <div class="panel panel-default"><div class="panel-body">
    <h1>Herramienta para extraer datos de SBS a partir del ISBN</h1>
    <a class="btn btn-default btn-primary" href='addjob?jobtype=sbs'>Agregar nuevo trabajo</a>
    <br>
    <br>
    <a class="btn btn-default btn-success" href='listjobs?jobtype=sbs'>Ver trabajos</a>
    <br>
    <br>
    <a class="btn btn-default btn-warning" href='purgejobs?jobtype=sbs'>Limpiar trabajos no terminados</a>
    <br>
    <br>
    <form class="form-inline" action="getisbndata">
      <div class="form-group">
        <span>Consultar un solo ISBN:</span>
        <br>
        <input type="text" class="form-control" name="isbn" placeholder="ISBN">
        <input type="hidden" class="form-control" name="jobtype" value="sbs">
        <br>
        <button type="submit" class="btn btn-default">Enviar</button>
      </div>
    </form>
    </div></div>

    <!--div class="panel panel-default"><div class="panel-body">
    <h1>Herramienta para extraer datos de Casa del Libro a partir del ISBN</h1>
    <a class="btn btn-default btn-primary" href='addjob?jobtype=casadellibro'>Agregar nuevo trabajo</a>
    <br>
    <br>
    <a class="btn btn-default btn-success" href='listjobs?jobtype=casadellibro'>Ver trabajos</a>
    <br>
    <br>
    <a class="btn btn-default btn-warning" href='purgejobs?jobtype=casadellibro'>Limpiar trabajos no terminados</a>
    <br>
    <br>
    <form class="form-inline" action="getisbndata">
      <div class="form-group">
        <span>Consultar un solo ISBN:</span>
        <br>
        <input type="text" class="form-control" name="isbn" placeholder="ISBN">
        <input type="hidden" class="form-control" name="jobtype" value="casadellibro">
        <br>
        <button type="submit" class="btn btn-default">Enviar</button>
      </div>
    </form>
    </div></div-->

    <div class="panel panel-default"><div class="panel-body">
    <h1>Control de scrapers</h1>
    {% for scraper in scrapers  %}
        <h2>Scraper : {{ scraper.name }}</h2>
        <span>Satus :
        {% if scraper.status %}
        <span class="label label-success">Running</span>
        {% else %}
        <span class="label label-danger">Not running</span>
        {% endif %}
        </span>
        <br>
        <br>
        {% if scraper.properties %}
            <!--span>Available actions:</span-->
            {% if "all" in scraper.properties.canget %}
                <a class="btn btn-default btn-success" href="/startscraper?id={{ scraper.name }}">Dump all data</a>
            {% endif %}
            {% if "bestsellers" in scraper.properties.canget %}
                <a class="btn btn-default btn-success" href="/startscraper?id={{ scraper.name }}&target=bestsellers">Dump best sellers</a>
            {% endif %}
            {% if "novelties" in scraper.properties.canget %}
                <a class="btn btn-default btn-success" href="/startscraper?id={{ scraper.name }}&target=novelties">Dump novelties</a>
            {% endif %}
        {% else %}
            <a class="btn btn-default btn-success" href="/startscraper?id={{ scraper.name }}">Start</a>
        {% endif %}
        <a class="btn btn-default btn-danger" href="/killscraper?id={{ scraper.name }}">Abort</a>
        <br>
        <h3>Resultados disponibles para bajar:</h3>
        <div style="overflow: auto; height: 200px;">
            <table class="table">
                {% for file in scraper.files  %}
                <tr>
                    <td>{{ file.name }}</td>
                    <td><a class="btn btn-default btn-success" href='/downloadscraperresults?fmt=csv&scrapername={{ scraper.name }}&fileid={{ file.id }}'>CSV</a></td>
                    <td><a class="btn btn-default btn-success" href='/downloadscraperresults?fmt=xls&scrapername={{ scraper.name }}&fileid={{ file.id }}'>XLS</a></td>
                </tr>
                {% endfor %}
            </table>
        </div>
    <hr>
    {% endfor %}
    </div></div>
</body>
</html>
