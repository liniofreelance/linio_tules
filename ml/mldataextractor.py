#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Entrypoint to run the extractor ppipeline.
Sample calls:
python2.7 mldataextractor.py -l
python2.7 mldataextractor.py -w
python2.7 mldataextractor.py -d MLA1253 MLA1072 -m 10
python2.7 mldataextractor.py -c "Informacion por Categorias 14-07-2015_0.xlsx"
python2.7 mldataextractor.py -c "Informacion por Categorias 16-06-2019.xlsx"
"""

from __future__ import print_function  # -*- coding: utf-8 -*-

import argparse
import datetime
import os
import sys

from mldataextractor_web import startWebServer
from mldataextractor_impl import (
    getCategoryFile,
    consolidateResults,
    load_cat_code_names_lookup,
    MAX_ITEMS,
)


def checkFixFnPreventOverwrite(fn):
    if not os.path.exists(fn):
        return fn
    fnparts = os.path.splitext(fn)
    for i in range(99999):
        newfn = "%s_%d%s" % (fnparts[0], i, fnparts[1])
        if not os.path.exists(newfn):
            return newfn
    return None


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-w", "--web", help="Run webserver", action="store_true")
    parser.add_argument(
        "-d", "--download", metavar="CATCODE", nargs="+", help="Categories to download"
    )
    parser.add_argument(
        "-c", "--consolidate", help="Consolidate sheets on input files into outputfile"
    )
    parser.add_argument(
        "-m",
        "--maxitems",
        type=int,
        default=MAX_ITEMS,
        help="Maximun number of items to download per category (deafult 100)",
    )
    parser.add_argument("-o", "--outfn", default=None, help="Output filename")
    # parser.add_argument("-l", "--list", action="store_true", help="List available categories")
    args = parser.parse_args()

    if args.web:
        print("Running webserver")
        startWebServer()
        sys.exit()

    if args.download:
        outfn = (
            args.outfn
            if args.outfn
            else "Informacion por Categorias %s.xlsx"
            % datetime.datetime.today().strftime("%d-%m-%Y")
        )
        outfn = checkFixFnPreventOverwrite(outfn)
        catnamesncodes_todownload = []
        catnames_lookup = load_cat_code_names_lookup()
        for catcode in args.download:
            if catcode in catnames_lookup:
                catnamesncodes_todownload += [(catnames_lookup[catcode], catcode)]
            else:
                print("Category not found: %s" % catcode)
        print(outfn, catnamesncodes_todownload, args.maxitems)
        getCategoryFile(outfn, catnamesncodes_todownload, args.maxitems)
        sys.exit()

    if args.consolidate:
        infn = (
            args.consolidate
            if args.consolidate
            else "Informacion por Categorias %s.xlsx"
            % datetime.datetime.today().strftime("%d-%m-%Y")
        )
        outfn = (
            args.outfn
            if args.outfn
            else "Consolidado Actualizado %s.xlsx"
            % datetime.datetime.today().strftime("%d-%m-%Y")
        )
        outfn = checkFixFnPreventOverwrite(outfn)
        print(infn, outfn)
        consolidateResults(infn, outfn)
        sys.exit()

    # if args.list :
    #    for (n, c) in catnamesncodes :
    #        print '"%s","%s"' % (n, c)
    #    sys.exit()


if __name__ == "__main__":
    main()
