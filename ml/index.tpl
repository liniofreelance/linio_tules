<!doctype html>
<html lang="en">
<head>
  <title>ML Data Extractor</title>
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <!-- Optional theme -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>
  <div class="container">
     <h1>Herramienta de extraccion de datos de MercadoLibre</h1>
  </div>
  <div class="container">
    <div class="col-md-6">
       <h4>Procesos de extraccion de datos que se encuentran corriendo ({{ extractors_status.count }})</h4>
       <ul class="list-group">
          {% for extractor_cmd in extractors_status.cmds %}
          <li class="list-group-item">
             {{ extractor_cmd }}
          </li>
          {% endfor %}
       </ul>
       <br>
       <!--a href="http://datatules.no-ip.info:8091/" class="btn btn-success">Ver resultados</a-->
       <a href="killextractors" class="btn btn-danger">Cancelar todos</a>
    </div>
    <div class="col-md-6">
       <h4>Ingresar categorias para buscar de a una por linea</h4>
       <form action="validate_cats_and_launch" method="POST">
          <div class="row">
             <textarea name='catcodes' class="form-control" rows=20 cols=20></textarea>
          </div>
          <br>
          <div class="row">
             <label for="maxitems">Cantidad maxima de items por categoria:</label>
             <input name='maxitems' id='maxitems' class="form-control" value=20>
          </div>
          <div class="row">
             <input class="btn btn-default" type='submit' value='Iniciar'>
          </div>
       </form>
    </div>
  </div>
  <div class="container">
     <div class="row">
        <h3>Resultados disponibles para bajar:</h3>
        <div style="overflow: auto; height: 200px;">
           <table class="table table-striped">
              {% for file in scraper.files  %}
              <tr>
                 <td>{{ file.name }}</td>
                 <td><a class="btn btn-default btn-success" href='./downloadscraperresults?fmt=xls&scrapername={{ scraper.name }}&fileid={{ file.id }}'>XLSX</a></td>
              </tr>
              {% endfor %}
           </table>
        </div>
     </div>
  </div>
</body>
</html>