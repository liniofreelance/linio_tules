<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    </head>
    <body>
    <div class="panel panel-default"><div class="panel-body">
        <h1>Agregar trabajo nuevo:</h1>
        <form class="form-inline" action="addnewjob" method="POST" enctype="multipart/form-data">
          <div class="form-group">
            <input type="hidden" class="form-control" name="jobtype" value="{{ jobtype }}">
            <span>Nombre:</span>
            <br>
            <input type="text" class="form-control" name="name" placeholder="Nombre del trabajo">
            <br>
            <br>
            <span>Listado de ISBNs a descargar:</span>
            <br>
            <textarea class="form-control" name="isbnlist" placeholder="ISBNs" multiline="true" cols=25 rows="20"></textarea>
            <br>
            <br>
            <span>Cargar por archivo:</span>
            <br>
            <input type="file" name="isbnfile" />
            <br>
            <button type="submit" class="btn btn-default">Enviar</button>
          </div>
        </form>
        <a class="btn btn-default" href="/">Volver</a>
        </div></div>
    </body>
</html>
