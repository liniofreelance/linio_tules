# -*- coding: utf-8 -*-

import scrapy
import json
from scrapy.http import Request
from linio_scrapers.items import GenericListingItem, GenericItem, GenericCategoryListingItem
from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst, MapCompose, Join

"""
scrapy crawl megustaleer -a target=item -a targetinp="http://www.megustaleer.com.ar/libros/born/9789500752428"

#scrapy crawl megustaleer -a target=dumplisting -a targetinp="http://www.megustaleer.com.ar/search.php?facet=UBI_HOM@TEM_73@"
"""

def GetFirst(l,v=None) :
    if len(l)>=1 :
        v = l[0]
    if type(v) == str :
        v = v.strip()
    return v

class MegustaleerSpider(scrapy.Spider):
    name = "megustaleer"
    baseurl = "http://www.megustaleer.com.ar/"
    allowed_domains = ["www.megustaleer.com.ar"]
    
    target = None
    
    def __init__(self, target="dumpall", followbooks="yes", targetinp=None):
        #import time; time.sleep(99999)
        self.target = target
        self.start_urls = [targetinp]
        if self.target in ["dumpall"] :
            self.start_urls = ["http://www.megustaleer.com.ar/ajax/search_result.php?vista=list&pagenum=999999"]
            self.parse = self.dumpAll
        if self.target in ["dumplisting"] :
            self.start_urls = [targetinp]
            self.parse = self.parseListing
        if self.target == "item" :
            self.start_urls = [targetinp]
            self.parse = self.parseItem
    
    def dumpAll(self, response) :
        maxpage = int(response.css("a.paginador::text").extract()[-2])
        for i in range(1,maxpage+1) :
            yield Request("http://www.megustaleer.com.ar/ajax/search_result.php?vista=list&pagenum=%d" % i, callback=self.parseListing)
    
    def parseListing (self, response) :
        response.css("div.content.book-detail-1 > h4 > a::attr(href)").extract()
        for url in response.css("div.content.book-detail-1 > h4 > a::attr(href)").extract() :
            yield Request(url.strip(), callback=self.parseItem)
            
    def parseItem (self, response) :
        l = ItemLoader(item=GenericItem(), response=response)
        
        l.add_value("url", response.url)
        l.add_xpath("desc", "//blockquote[@itemprop='text']/text()", TakeFirst())
        
        video_trailer_id = response.css("a.video-youtube::attr(data-id)").extract()
        if len(video_trailer_id) > 0:
            l.add_value("video_trailer", "http://youtube.com/v/%s" % video_trailer_id[0])
        else :
            video_src = response.css("div.row").xpath(".//iframe[contains(@src,youtube)]/@src").extract()
            if len(video_src) > 0 :
                l.add_value("video_trailer", "http://youtube.com/v/%s" % video_src[0].split("/")[4])
            
        
        l.add_xpath("img", "//img[@itemprop='image']/@data-original", TakeFirst())
        if len(response.css("a.imagen_alta")) > 0 :
            isbn = response.url.split("/")[-1]
            img_hd_url = "http://images.megustaleer.com.ar/libros_max/%s.jpg" % isbn
            l.add_value("img_hd", img_hd_url)
        
        try :
            backcover_text = response.css("div#contratapa-long > p::text").extract()[0].replace("\n","<br>")
            l.add_value("backcover_text", backcover_text)
            #l.add_css("backcover_text", , TakeFirst())
        except :
            pass

        sel = response.css("div#ficha-libro > div > div > ul > li")
        kvs = dict(zip(sel.xpath("./span/text()").extract(), sel.xpath("./text()").extract()))
        fields = {
            u'Autor (es):': 'author',
            #u'Colecci\xf3n:': u' Historia',
            #u'EAN:': u' 9789500753982',
            #u'Edad recomendada:': u' E-book',
            u'Fecha publicaci\xf3n:': 'pubdate',
            #u'Formato, p\xe1ginas:': u' E-BOOK EPUB, 408',
            u'ISBN:': 'isbn',
            u'Idioma:': 'lang',
            u'Medidas:': 'dimensions',
            u'Precio con IVA:': 'prices',
            #u'Precio sin IVA:': u' $\xa0124,99',
            u'Sello:': u'publisher',
            u'Tem\xe1ticas:': 'cat',
            #u'Traductor:': u' \xa0',
            u'T\xedtulo:': 'title'
        }
        ignore_fields = [
            u'Formato, p\xe1ginas:',
        ]
        
        misc = {}
        for (k,v) in kvs.items() :
            if k in ignore_fields :
                continue
            if fields.has_key(k) :
                l.add_value(fields[k], v.strip())
            else :
                misc[k] = v
        
        l.add_value("miscdata", json.dumps(misc))
        
        if kvs.has_key(u'Formato, p\xe1ginas:') :
            try :
                vs = kvs[u'Formato, p\xe1ginas:'].split(",")
                l.add_value("format", vs[0].strip())
                l.add_value("pages", vs[1].strip())
            except :
                pass

        frament_url = response.xpath("//a[contains(@href,'fragmento')]/@href").extract()
        if len(frament_url) > 0 :
            #l.add_value("extract", frament_url[0])
            request = Request(frament_url[0], callback=self.extractFramentData)
            request.meta['l'] = l
            return request
        else :
            return l.load_item()
    
    def extractFramentData (self, response) :
        l = response.meta["l"]
        extract = response.css("div#capitulo-preview").extract()[0].replace("\n","")
        l.add_value("extract", extract)
        return l.load_item()
        
        
               
        
