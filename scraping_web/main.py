# -*- coding: utf-8 -*-

from isbn_to_amazon import getDataFromAmazonUsingISBN
from isbn_to_sbs import getDataFromSBSUsingISBN
from isbn_to_casadellibro import getDataFromCasaDelLibroUsingISBN
import isbnconverter
import tasks
#import helpers
import file_format_helper

import datetime
import sys
import os
import os.path
import csv
import xlsxwriter
import json

import cherrypy
from cherrypy.lib.static import serve_file
from cherrypy.lib import auth_basic
from jinja2 import Template
from pprint import pprint
import tempfile

from pymongo import MongoClient
from bson.objectid import ObjectId

client = MongoClient()
db = client.lineo
#jobscoll = db.jobs
#itemcoll = db.items

USERS = {'linio' : 'j9u437t5'}

testtask = {
    "isbn" : "dsadsad",
    "status" : "pending"
}

testjob = {
    "id":111,
    "name":"PEPE", 
    "starttime":11, 
    "status":"completed",
    "tasks":[testtask],
}

# Cuidado de no meter caracteres raros aca
available_scrapers = {
"buscalibre" : {},
"cuspide" : {
    "canget" : ["all", "bestsellers", "novelties"]
},
"tematika" : {
    "canget" : ["all", "bestsellers", "novelties"]
},
"casassaylorenzo" : {},
"kel" : {},
"casadellibro" : {
    "canget" : ["all"]
},
"megustaleer" : {
    "canget" : ["all"]
},
}


curdir = os.path.dirname(os.path.realpath(__file__))
resultdir = curdir + "/../../scraper_results/"
formatted_output_dir = curdir + "/../../formatted_results/"
    
for d in [formatted_output_dir, resultdir] :
    if not os.path.exists (d) :
        os.mkdir(d)

class ScraperManager() :
    name = None
    properties = None
    
    curdir = os.path.dirname(os.path.realpath(__file__))
    scrapypath = curdir + "/../linio_scrapers/"
    #scrapypath = "/home/w1ndman/Dropbox/linio/repo/scraping_web/linio_scrapers"
    resultdir = resultdir
    formatted_output_dir = formatted_output_dir

    def __init__(self, name, properties):
        self.name = name
        self.properties = properties
    
    def run(self, target) : 
        if self.isRunning() :
            return
        
        outfn = "%s/%s__%s.json" % (self.resultdir, self.name, datetime.datetime.now().strftime("%d_%m_%y_%H:%M:%S"))
        target_arg = " -a target=%s " % target if target != None else ""
        scrapy_cmd = "cd %s; scrapy crawl %s -o %s %s" % (self.scrapypath, self.name, outfn, target_arg)
        formatter_cmd = "cd %s; python file_format_helper.py --infn %s --outdir %s" % (self.curdir, outfn, self.formatted_output_dir)
        cmd = "nohup bash -c \"(%s; %s)\" &" % (scrapy_cmd, formatter_cmd)
        print cmd
        os.system(cmd)

    def isRunning(self) : 
        return os.system("ps ax | grep -v grep | grep scrapy | grep -q %s" % self.name) == 0

    def kill (self): 
        os.system("kill $(ps ax | grep -v grep | grep scrapy | grep %s | awk '{print $1}')" % self.name)

    def getRunningTime(self):
        #return os.popen("ps -fea | grep -v grep | grep scrapy | grep %s | awk '{print $5}'" % self.name).read().strip()
        #return os.popen("ps -p $(ps ax | grep -v grep | grep scrapy | grep %s | awk '{print $1}') -o etime=" % self.name).read()
        return "--:--"

    def listAvailableResults(self):
        available_results = [fn for fn in os.listdir(self.resultdir) if fn[:len(self.name)] == self.name]
        available_results.sort(key=lambda x: os.stat(os.path.join(self.resultdir, x)).st_mtime, reverse=True)
        available_results = [(fn, str(hash(self.resultdir+fn+"gvu453789upihdr380") % ((sys.maxsize + 1) * 2))) for fn in available_results]
        print available_results
        return available_results

    def getFilenameFromId (self, id) :
        ret = [(fn,h) for (fn,h) in self.listAvailableResults() if h==id]
        return ret[0][0] if len(ret) >= 0 else None
    
scrapersMgmt = dict([(scrapername, ScraperManager(scrapername, properties)) for (scrapername, properties) in available_scrapers.items()])

class MainServer(object):
    @cherrypy.expose
    def index(self):
        scrapers = []
        for scraper in scrapersMgmt.values() :
            scrapers += [{
                "name" : scraper.name,
                "status" : scraper.isRunning(),
                "id" : scraper.name,
                "properties" : scraper.properties,
                "runningtime" : scraper.getRunningTime(),
                "files" : [{"name":e[0], "id":e[1]} for e in scraper.listAvailableResults()]
            }]
        pprint(scrapers)
        t = Template(open("index.tpl").read()) ; print scrapers
        return t.render(scrapers=scrapers)

    @cherrypy.expose
    def startscraper(self, id, target=None):
        scrapersMgmt[id].run(target)
        raise cherrypy.HTTPRedirect("/")
        #return self.index()
    
    @cherrypy.expose
    def killscraper(self, id):
        scrapersMgmt[id].kill()
        raise cherrypy.HTTPRedirect("/")
        #return self.index()
    
    @cherrypy.expose
    def downloadscraperresults(self, scrapername, fileid, fmt) :
        fn = scrapersMgmt[scrapername].resultdir + "/" + scrapersMgmt[scrapername].getFilenameFromId(fileid)
        fieldnames = file_format_helper.collect_fieldnames_from_scrapy_json(fn)
        data = file_format_helper.get_scrapy_json_data_reader(fn)
        downloadfn = "%s.%s" % (os.path.splitext(os.path.basename(fn))[0], fmt)
        return self.serveAsFormat(fmt, fieldnames, data, downloadfn, cache=True)
    

    @cherrypy.expose
    def getisbndata(self, isbn, jobtype):
        getdatamethod = tasks.jobtypeconfs[jobtype]["getdatamethod"]
        data = [d for d in [getdatamethod(isbn)] if d!=None]
        print getdatamethod
        pprint(data)
        data = [self.flattenSuggestions (d) for d in data]
        
        fieldnames = reduce(lambda a,b: a.union(set(b)), data, set())
        fieldnames = sorted(list(fieldnames))
        lines = [ [d.get(h, "") for h in fieldnames] for d in data]
        t = Template(open("results.tpl").read())
        return t.render(headers=fieldnames, lines=lines)

    @cherrypy.expose
    def listjobs(self, jobtype):
        jobscoll = tasks.jobtypeconfs[jobtype]["jobscoll"]
        jobs = list(jobscoll.find())
        jobs.sort(key=lambda e:e["name"].lower())
        t = Template(open("listjobs.tpl").read())
        return t.render(jobs=jobs,jobtype=jobtype)

    @cherrypy.expose
    def viewjob(self, id, jobtype):
        import tasks
        jobscoll = tasks.jobtypeconfs[jobtype]["jobscoll"]
        itemcoll = tasks.jobtypeconfs[jobtype]["itemcoll"]
        job = jobscoll.find_one({"_id":ObjectId(id)}) 
        tasks = list(itemcoll.find({"isbn":{"$in":job["isbnlist"]}}))
        tasks.sort(key=lambda e:e["isbn"])
        job["tasks"] = tasks
        #pprint(job)
        #pprint(tasks)
        t = Template(open("viewjob.tpl").read())
        return t.render(job=job, jobtype=jobtype)

    @cherrypy.expose
    def addjob(self, jobtype):
        t = Template(open("addjob.tpl").read())
        return t.render(jobtype=jobtype)

    @cherrypy.expose
    def addnewjob(self, name, isbnlist=None, jobtype=None, isbnfile=None) :
        cherrypy.response.timeout = 3600
        
        jobscoll = tasks.jobtypeconfs[jobtype]["jobscoll"]
        itemcoll = tasks.jobtypeconfs[jobtype]["itemcoll"]
        taskmethod = tasks.jobtypeconfs[jobtype]["taskmethod"]
        
        if not isbnfile.file is None :
            print "Receiving file"
            size = 0
            isbnlist = ""
            while True:
                newdata = isbnfile.file.read(8192)
                if not newdata:
                    break
                size += len(newdata)
                isbnlist += newdata
            print "Received %d bytes" % size
        elif not isbnlist is None :
            pass
        else :
            isbnlist = ""
        
        if len(isbnlist) > 0 :
            isbnlist = map(lambda s:s.strip(), isbnlist.split("\n"))
            isbnlist = filter(lambda s:len(s)>0, isbnlist)
        print "len(isbnlist)=%d" % len(isbnlist)
        
        newjob = {
            "name" : name,
            "isbnlist" : isbnlist,
            "status" : "runninng"
        }
        print "Inserting new jobs in db"
        newtasklist = []
        for isbn in isbnlist :
            if itemcoll.find({"isbn": isbn}).count() == 0 :
                newtasklist += [{"isbn":isbn, "status":"new", "inserttime":datetime.datetime.now()}]
        if len(newtasklist) > 0 :
            itemcoll.insert_many(newtasklist)
        
        print "Inserting new jobs in queue"
        for isbn in isbnlist :
            taskmethod.delay(isbn)
        newjob["id"] = jobscoll.insert_one(newjob).inserted_id
        #pprint(newjob)
        t = Template(open("genericActionResult.tpl").read())
        return t.render(job=newjob, textSelector="add")

    @cherrypy.expose
    def canceljob(self, id, jobtype):
        jobscoll = tasks.jobtypeconfs[jobtype]["jobscoll"]
        itemcoll = tasks.jobtypeconfs[jobtype]["itemcoll"]
        
        job = jobscoll.find_one({"_id":ObjectId(id)})

        itemcoll.update({
                "isbn": { "$in":job["isbnlist"] },
                "status":"new"
            },
            {
                "$set" : { "status":"aborted" }
            },
            multi=True, upsert=False
        )

        job["status"] = "aborted"
        jobscoll.update({"_id":job["_id"]}, job, upsert=False)
        
        t = Template(open("genericActionResult.tpl").read())
        return t.render(job=job, textSelector="cancel")

    @cherrypy.expose
    def deletejob(self, id, jobtype):
        jobscoll = tasks.jobtypeconfs[jobtype]["jobscoll"]
        itemcoll = tasks.jobtypeconfs[jobtype]["itemcoll"]
        
        job = jobscoll.find_one({"_id":ObjectId(id)})
        
        itemcoll.update({
                "isbn": { "$in":job["isbnlist"] },
                "status":"new"
            },
            {
                "$set" : { "status":"aborted" }
            },
            multi=True, upsert=False
        )
        
        jobscoll.remove({"_id":ObjectId(id)})
        
        t = Template(open("genericActionResult.tpl").read())
        return t.render(job=job, textSelector="delete")

    def flattenSuggestions(self, data) :
        sugs = data.get("suggested",[])
        for (i, sug) in zip(range(len(sugs)), sugs) :
            try :
                data["suggested%d_link" % i] = sug["link"]
                data["suggested%d_title" % i] = sug["title"]
                data["suggested%d_img" % i] = sug["img"]
            except :
                pass
        if data.has_key("suggested") :
            del data["suggested"]
        return data

    def serveAsFormat (self, fmt, fieldnames, data, downloadfn, cache=False) :
        # Seria buena idea refactorizar el cacheo y formateo del archivo fuera de esta funcion 
        if not fmt in file_format_helper.available_formats.keys() :
            return None
        
        if cache :
            fn = "%s/%s" % (formatted_output_dir, downloadfn)
            cachehit = os.path.exists(fn)
        else :
            f = tempfile.NamedTemporaryFile(delete=False)
            fn = f.name
            cachehit = False
        
        print "fn : %s, cache : %s, cachehit : %s" % (fn, cache, cachehit)
        if not cache :
            file_format_helper.saveTo(fmt, fn, fieldnames, data)
        elif not cachehit :
            file_format_helper.saveTo(fmt, fn, fieldnames, data)
        
        res = serve_file(fn, disposition='attachment',
            content_type='.'+fmt, name=downloadfn)
        
        if not cache :
            os.unlink(fn)
        
        return res
    
    @cherrypy.expose
    def downloadjobresults(self, id, fmt, jobtype):
        if not fmt in ["csv", "xls"] :
            return None
        jobscoll = tasks.jobtypeconfs[jobtype]["jobscoll"]
        itemcoll = tasks.jobtypeconfs[jobtype]["itemcoll"]
        objdataattr = tasks.jobtypeconfs[jobtype]["objdataattr"]
        job = jobscoll.find_one({"_id":ObjectId(id)})
        items = list(itemcoll.find({"isbn" : {"$in":job["isbnlist"]}}))
        for i in range(len(items)) :
            items[i] = self.flattenSuggestions(items[i].get(objdataattr, {}))
        fieldnames = reduce(lambda a,b: a.union(set(b)), items, set())
        fieldnames = sorted(list(fieldnames))
        #pprint (items)
        return self.serveAsFormat(fmt, fieldnames, items, "%s.%s" % (job["name"], fmt))
    
    @cherrypy.expose
    def viewjobresults(self, id, jobtype):
        jobscoll = tasks.jobtypeconfs[jobtype]["jobscoll"]
        itemcoll = tasks.jobtypeconfs[jobtype]["itemcoll"]
        objdataattr = tasks.jobtypeconfs[jobtype]["objdataattr"]
        job = jobscoll.find_one({"_id":ObjectId(id)})
        items = list(itemcoll.find({"isbn" : {"$in":job["isbnlist"]}}))
        data = [item.get(objdataattr,{}) for  item in items]
        data = [{} if d is None else d for d in data]
        data = [self.flattenSuggestions (d) for d in data]
        
        fieldnames = reduce(lambda a,b: a.union(set(b)), data, set())
        fieldnames = sorted(list(fieldnames))
        lines = [ [d.get(h, "") for h in fieldnames] for d in data]
        t = Template(open("results.tpl").read())
        return t.render(headers=fieldnames, lines=lines, jobtype=jobtype)
    
    @cherrypy.expose
    def purgejobs(self, jobtype):
        jobscoll = tasks.jobtypeconfs[jobtype]["jobscoll"]
        itemcoll = tasks.jobtypeconfs[jobtype]["itemcoll"]
        objdataattr = tasks.jobtypeconfs[jobtype]["objdataattr"]
        for job in jobscoll.find({"status":"runninng"}) :
            jobscoll.remove({"_id":ObjectId(job["_id"])})
        
        for item in itemcoll.find({"status":"new"}) :
            itemcoll.remove({"_id":ObjectId(item["_id"])})
        
        return open("purgeres.html").read()

    @cherrypy.expose
    def listscrapers(self):
        t = Template(open("scrapers.tpl").read())
        return t.render(headers=fieldnames, lines=lines)


def validate_password(realm, username, password):
    return True
    if username in USERS and USERS[username] == password:
       return True
    return False

import sys
sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + "/../ml/")

import mldataextractor_web

if __name__ == "__main__" :
    config = {
        #'server.socket_host': '0.0.0.0',
        '/': {
            'tools.auth_basic.on': True,
            'tools.auth_basic.realm': 'localhost',
            'tools.auth_basic.checkpassword': validate_password
        }
    }
    cherrypy.config.update({'server.socket_host': '0.0.0.0'}) 
    #cherrypy.quickstart(MainServer(), "/", config=config)
    #cherrypy.quickstart(mldataextractor.MainWebServer(), "/ml", config=config)
    cherrypy.tree.mount(MainServer(), "/", config)
    cherrypy.tree.mount(mldataextractor_web.MainWebServer(), "/ml", config)
    cherrypy.engine.start()
    cherrypy.engine.block()
    




