


import scrapy
from linio_scrapers.items import CuspideListingItem, CuspideItem
from helpers import GetFirst

class CuspideSpider(scrapy.Spider):
    name = "cuspide"
    baseurl = "http://www.cuspide.com/"
    allowed_domains = ["www.cuspide.com"]
    start_urls = [
        #"http://www.dmoz.org/Computers/Programming/Languages/Python/Books/",
        #"http://www.dmoz.org/Computers/Programming/Languages/Python/Resources/"
    ]
    
    target = None
    followbooks = False
    
    def __init__(self, target='dumpall', followbooks="yes" ,targetinp=None):
        self.target = target
        if self.target == "dumpall" :
            self.setupDumpAll()
        if self.target == "novelties" :
            self.setupDumpNovelties()
        if self.target == "bestsellers" :
            self.setupDumpBestsellers()
        if self.target == "singlebook" :
            #self.start_urls = ["http://www.cuspide.com/9789876372688/Huerta+Y+Cocina/"]
            self.start_urls = targetinp if targetinp != None else "http://www.cuspide.com/9789876372688/Huerta+Y+Cocina/"
            self.parse = self.parsebookpage
        if self.target == "bookbatch" :
            if targetinp == None :
                print "Define targets first!"
            self.start_urls = [fn.strip() for fn in open(targetinp).readlines()]
            self.parse = self.parsebookpage
    
    def setupDumpAll (self) :
        self.start_urls = ["http://www.cuspide.com/librosportema/"]
        self.parse = self.parsecategorypage
    
    def setupDumpNovelties (self) :
        self.start_urls = ["http://www.cuspide.com/novedades/"]
        self.parse = self.parselistingfollowlinks

    def setupDumpBestsellers (self) :
        self.start_urls = ["http://www.cuspide.com/bestsellers/"]
        self.parse = self.parselistingfollowlinks

    def parse(self, response) :
        pass
    
    def parselistingfollowlinks (self, response) :
        for e in response.xpath("//nav[@class='novedadesTema']/a") :
            url = GetFirst(e.xpath("./@href"))
            linkname = GetFirst(e.xpath("./text()"))
            yield scrapy.Request(self.baseurl+url, callback=self.parselisting)
    
    def parsecategorypage (self, response) :
        for url in [l.strip() for l in response.xpath("//div[@class='selectStyledFilters']//option[@value!='']/@value").extract()] :
            #print url
            yield scrapy.Request(self.baseurl+url, callback=self.parselisting)
    
    def parselisting (self, response) :
        for libro in response.css("article.libro") :
            item = CuspideListingItem()
            item["title"] = GetFirst(libro.xpath(".//h1/a/text()"))
            item["url"] = GetFirst(libro.xpath(".//h1/a/@href"))
            item["img"] = GetFirst(libro.xpath(".//figure//img/@src"))
            item["cat"] = GetFirst(libro.xpath(".//div[@class='cat']//a/text()"))
            item["resenia"] = GetFirst(libro.xpath(".//*[@class='resenia']/text()"))
            prices = libro.xpath(".//div[@class='precio']")
            prices = dict(zip(prices.xpath(".//div/span/text()").extract(), prices.xpath(".//div/text()").extract()))
            item["prices"] = prices
            #yield item
            if self.target in ["dumpall", "novelties", "bestsellers"] :
                yield scrapy.Request(self.baseurl+item["url"], callback=self.parsebookpage)
        
        for url in response.xpath("//div[@class='paginado desktop']//a/@href").extract() :
            yield scrapy.Request(self.baseurl+url, callback=self.parselisting)
    
    def parsebookpage (self, response) :
        title = GetFirst(response.xpath("//h1[@itemprop='name']/a/text()"))
        author = GetFirst(response.xpath("//*[@itemprop='author']//*[@itemprop='name']/text()"))
        publisher = GetFirst(response.xpath("//*[@itemprop='publisher']//*[@itemprop='name']/text()"))
        
        prices = response.xpath("//div[@class='precio']")
        prices = dict(zip(prices.xpath(".//div/span/text()").extract(), prices.xpath(".//div/text()").extract()))
        
        miscdata = response.xpath("//div[@class='info']//li")
        ks = [k.replace(":","") for k in miscdata.xpath(".//label/text()").extract()]
        vs = miscdata.xpath(".//span/text()").extract()
        miscdata = dict(zip(ks,vs))
        
        cat = response.xpath("//div[@class='cat']//a/text()").extract()
        
        desc = response.xpath("//*[@itemprop='description']//text()").extract()
        desc = [l.strip() for l in desc]
        desc = '\n'.join(desc)
        
        img = GetFirst(response.xpath("//img[@itemprop='image']/@src"))
        
        item = CuspideItem()
        item["title"] = title
        item["author"] = author
        item["publisher"] = publisher
        item["prices"] = prices
        item["miscdata"] = miscdata
        item["cat"] = cat
        item["desc"] = desc
        item["img"] = img
        item["url"] = response.url
        return item
        
