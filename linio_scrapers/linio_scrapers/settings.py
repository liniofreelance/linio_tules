# -*- coding: utf-8 -*-

# Scrapy settings for linio_scrapers project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'linio_scrapers'

SPIDER_MODULES = ['linio_scrapers.spiders']
NEWSPIDER_MODULE = 'linio_scrapers.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'linio_scrapers (+http://www.yourdomain.com)'

CONCURRENT_REQUESTS_PER_IP = 1
DOWNLOAD_DELAY = 0.25
