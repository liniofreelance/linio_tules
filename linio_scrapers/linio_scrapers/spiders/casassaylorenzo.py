 # -*- coding: utf-8 -*-
 
import os.path
import HTMLParser
from lxml import html
import json

import scrapy
from scrapy.http import Request
from linio_scrapers.items import GenericListingItem, GenericItem, GenericCategoryListingItem
from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst, MapCompose, Join

def GetFirstStrip(l, d="") :
    if len(l) == 0 :
        return d
    return l[0].strip()

class CasassaylorenzoSpider(scrapy.Spider):
    name = "casassaylorenzo"
    baseurl = "http://www.casassaylorenzo.com/"
    allowed_domains = ["www.casassaylorenzo.com"]
    #start_urls = [
    #    "http://http://www.casassaylorenzo.com/"
    #]
    
    target = None
    
    def __init__(self, target="dumpall", followbooks="yes", targetinp=None):
        #import time; time.sleep(99999)
        self.target = target
        self.start_urls = [targetinp]
        if self.target in ["dumpall", "dumpcategories"] :
            self.start_urls = ["http://www.casassaylorenzo.com/temas.aspx"]
            self.parse = self.parseCategories
        if self.target in ["dumplisting", "dumplistingbooks", "dumplistingcontents"] :
            self.parse = self.parseListing
        if self.target == "item" :
            self.parse = self.parseItem
    
    #def parse (self, response) :
    def parseCategories (self, response) :
        catnames = response.xpath(".//ul[@class='temacolumn']//a/text()").extract()
        catalogurls = response.xpath(".//ul[@class='temacolumn']//a/@href").extract()
        for url in catalogurls :
            yield Request(self.baseurl+url, callback=self.parseListing)
            #return
    
    def parseListing (self, response) :
        for listingitem in response.xpath("//div[@class='busqitem']") :
            link = listingitem.xpath(".//a[@class='itemtitulo']/@href").extract()[0]
            yield Request(self.baseurl+link, callback=self.parseItem)
            #return

        for url in response.xpath("//div[@class='numeros']//a/@href").extract() :
            yield Request(self.baseurl+url, callback=self.parseListing)
    
    def parseItem (self, response) :
        itemdata = {
            "url" : response.url,
            "img" : GetFirstStrip(response.xpath("//img[@name='tapaprodu']/@src").extract()),
            "title" : GetFirstStrip(response.css("h1.detalletitulo > a::text").extract()),
            "author" : GetFirstStrip(response.xpath("//*[@itemprop='author']//a/text()").extract()),
            "cat" : GetFirstStrip(response.xpath("//div[@class='detalleinfo']//a[contains(@id,'categoria')]/text()").extract()),
            "publisher" : GetFirstStrip(response.xpath("//*[@itemprop='publisher']//a/text()").extract()),
            "isbn" : GetFirstStrip(response.xpath("//meta[@itemprop='isbn']/@content").extract()),
            "prices" : GetFirstStrip(response.xpath("//span[@class='comprarcontprecio']/text()").extract()),
            "desc" : '\n'.join(map(lambda e:e.strip(), response.xpath("//div[@class='produtexto']//*/text()").extract())),
            "availavility" : GetFirstStrip(response.xpath("//*[@class='comprarcontprecio']/a/text()").extract()),
        }
        
        newGenericItem =  GenericItem(**itemdata)
        return newGenericItem





