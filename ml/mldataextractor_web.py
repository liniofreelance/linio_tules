# -*- coding: utf-8 -*-

from __future__ import print_function  # -*- coding: utf-8 -*-

import datetime
import subprocess
import os
import sys

import cherrypy
from cherrypy.lib.static import serve_file
from jinja2 import Template


execdir = os.path.dirname(os.path.abspath(__file__))
webserver_tpl_dir = execdir + "/"
webserver_taskres_dir = execdir + "/../../mlresults/"
mldataextractor_cli = execdir + "/mldataextractor.py"

# Este es el mismo codigo que hay en scrapper_web/main.py
class MLExtractorScraperManager:
    name = "MLDataExtractor"
    # properties = None
    # scrapypath = os.path.dirname(os.path.realpath(__file__)) + "/../linio_scrapers/"
    # scrapypath = "/home/w1ndman/Dropbox/linio/repo/scraping_web/linio_scrapers"
    resultdir = webserver_taskres_dir

    def __init__(self):
        pass

    """
    def __init__(self, name, properties):
        self.name = name
        self.properties = properties
        
    def run(self, target) : 
        if self.isRunning() :
            return
        # falta setear el output
        outfn = "%s/%s__%s.json" % (self.resultdir, self.name, datetime.datetime.now().strftime("%d_%m_%y_%H:%M:%S"))
        if target != None :
            cmd = "cd %s; nohup scrapy crawl %s -o %s -a target=%s &" % (self.scrapypath, self.name, outfn, target)
        else :
            cmd = "cd %s; nohup scrapy crawl %s -o %s &" % (self.scrapypath, self.name, outfn)
        print(cmd)
        os.system(cmd)
        #return os.system("nohup sleep 1000 &")
    def isRunning(self) : 
        return os.system("ps ax | grep -v grep | grep scrapy | grep -q %s" % self.name) == 0

    def kill (self): 
        os.system("kill $(ps ax | grep -v grep | grep scrapy | grep %s | awk '{print $1}')" % self.name)

    def getRunningTime(self):
        #return os.popen("ps -fea | grep -v grep | grep scrapy | grep %s | awk '{print $5}'" % self.name).read().strip()
        #return os.popen("ps -p $(ps ax | grep -v grep | grep scrapy | grep %s | awk '{print $1}') -o etime=" % self.name).read()
        return "--:--"
    """

    def listAvailableResults(self):
        # print os.listdir(self.resultdir), self.resultdir
        # available_results = [fn for fn in os.listdir(self.resultdir) if fn[:len(self.name)] == self.name]
        available_results = [
            fn for fn in os.listdir(self.resultdir) if fn[-len(".xlsx") :] == ".xlsx"
        ]
        # available_results = [fn for fn in os.listdir(self.resultdir)]
        available_results.sort(
            key=lambda x: os.stat(os.path.join(self.resultdir, x)).st_mtime,
            reverse=True,
        )
        available_results = [
            (
                fn,
                str(
                    hash(self.resultdir + fn + "gvu453789upihdr380")
                    % ((sys.maxsize + 1) * 2)
                ),
            )
            for fn in available_results
        ]
        # print available_results
        print("Listed %s available results" % len(available_results))
        return available_results

    def getFilenameFromId(self, id):
        ret = [(fn, h) for (fn, h) in self.listAvailableResults() if h == id]
        return ret[0][0] if len(ret) >= 0 else None


scraper = MLExtractorScraperManager()


class MainWebServer:
    @cherrypy.expose
    def index(self):
        # Muestro el main y un link a la instancia de simplehttpserver donde se pueda bajar todo
        # instcount = int(strip(subprocess.check_output(['ps -fea | grep "mldataextractor.py -d" | wc -l'], shell=True)))
        try:
            extractorprocs = (
                subprocess.check_output(
                    "ps -eo cmd | grep 'mldataextractor.py -d' | grep -v grep",
                    shell=True,
                )
                .strip()
                .split("\n")
            )
        except Exception as e:
            print("Got exception %s" % e)
            extractorprocs = []
        extractors_status = {"cmds": extractorprocs, "count": len(extractorprocs)}
        # return tpl.render(catnamesncodes=[(n.decode("utf8"), c.decode("utf8")) for (n,c) in catnamesncodes], extractors_status=extractors_status)

        scraper_data = {
            "name": scraper.name,
            # "status" : scraper.isRunning(),
            # "id" : scraper.name,
            # "properties" : scraper.properties,
            # "runningtime" : scraper.getRunningTime(),
            "files": [
                {"name": e[0], "id": e[1]} for e in scraper.listAvailableResults()
            ],
        }

        tpl = Template(open(webserver_tpl_dir + "index.tpl").read())
        return tpl.render(extractors_status=extractors_status, scraper=scraper_data)

    @cherrypy.expose
    def downloadscraperresults(self, scrapername, fileid, fmt):
        downloadfn = scraper.getFilenameFromId(fileid)
        fn = webserver_taskres_dir + "/" + downloadfn
        res = serve_file(
            fn, disposition="attachment", content_type=".xlsx", name=downloadfn
        )
        return res
        # data = json.load(open(fn))
        # fieldnames = reduce(lambda a,b: a.union(set(b)), data, set())
        # fieldnames = sorted(list(fieldnames))
        # print(fn)
        # if fmt == "csv" :
        #    return self.serveCSV(fieldnames, data, os.path.split(fn)[1].replace('.json',"")+".csv")
        # if fmt == "xls" :
        #    return self.serveXLSX(fieldnames, data, os.path.split(fn)[1].replace('.json',"")+".xls")

    @cherrypy.expose
    def validate_cats_and_launch(self, catcodes, maxitems=2):
        catcodes = catcodes.split()
        # valid, invalid = [], []
        # for catcode in catcodes :
        #    if catcode in CATNAMESNCODES_LOOKUP:
        #        targetlist = valid
        #    else:
        #        targetlist = invalid
        #    targetlist += [catcode]
        # print(valid, invalid)
        # if len(invalid) == 0 :
        #    return self.launch_work(catcodes, maxitems)
        # else :
        #    tpl = Template(open("validate_cats.tpl").read())
        #    #return tpl.render(valid=valid, invalid=invalid, catcodes=catcodes, maxitems=maxitems)
        #    return tpl.render(valid=valid, invalid=invalid, catcodes=catcodes, maxitems=maxitems)
        return self.launch_work(catcodes, maxitems)

    @cherrypy.expose
    def launch_work(self, catcodes, maxitems=2):
        maxitems = int(maxitems)
        # catcodes = catcodes.split()
        catfn = webserver_taskres_dir + "Informacion por Categorias %s.xlsx" % datetime.datetime.today().strftime(
            "%d-%m-%Y_%H:%M"
        )
        consfn = webserver_taskres_dir + "Consolidado Actualizado %s.xlsx" % datetime.datetime.today().strftime(
            "%d-%m-%Y_%H:%M"
        )
        catcmd = '/usr/bin/python %s -d %s -m %d -o "%s"' % (
            mldataextractor_cli,
            " ".join(catcodes),
            maxitems,
            catfn,
        )
        conscmd = '/usr/bin/python %s -c "%s" -o "%s"' % (
            mldataextractor_cli,
            catfn,
            consfn,
        )
        cmd = "%s ; %s" % (catcmd, conscmd)
        print(cmd)
        subprocess.Popen(cmd, shell=True)
        tpl = Template(open(webserver_tpl_dir + "return.tpl").read())
        return tpl.render(msg="Extractor iniciado")

    @cherrypy.expose
    def killextractors(self):
        cmd = "kill -9 $(ps -fea | grep 'mldataextractor.py -d' | grep -v grep | awk '{print $2}')"
        subprocess.call(cmd, shell=True)
        tpl = Template(open(webserver_tpl_dir + "return.tpl").read())
        return tpl.render(msg="Todos los extractores fueron cancelados")


def startWebServer():
    cherrypy.config.update(
        {"server.socket_host": "0.0.0.0", "server.socket_port": 8081}
    )
    cherrypy.quickstart(MainWebServer())
