import json
import requests
from scrapy.http import Request, HtmlResponse

import isbnconverter

def GetFirstStrip(l, d="") :
    if len(l) == 0 :
        return d
    return l[0].strip()

def getDataFromSBSUsingISBN(isbn) :
    #isbn = isbnconverter.toI10(isbn)
    urltpl = "http://sbs.com.ar/catalogsearch/result/?q=%s"
    url = urltpl % isbn
    req = requests.get(url)
    return getDataFromSBSUsingISBN_parsecontent(url, isbn, req.content)

def getDataFromSBSUsingISBN_parsecontent(url, isbn, content) :
    response = HtmlResponse(url=url, body=content)
    try :
        if len(response.xpath("//p[@class='note-msg empty-catalog']")) > 0 :
            datatable = response.css("table.data-table")[0]
    except :
        return {
            "isbn" : isbn
        }

    itemdata = {
        #"url" : response.url,
        "url" : GetFirstStrip(response.xpath("//comments/@href").extract()),
        "title" : GetFirstStrip(response.css("div.product-name > h1::text").extract()),
        "img" : GetFirstStrip(response.css("a#zoom-btn::attr(href)").extract()),
        "desc" : GetFirstStrip(response.xpath("//div[./h2/text()='Details']").css(".std::text").extract()),
        "price" : GetFirstStrip(response.css("span.regular-price > span.price::text").extract())
    }

    datatable = response.css("table.data-table")[0]
    kvtable = {
        "Autor" : "author",
        "Editorial" : "publisher",
        "ISBN" : "isbn",
        "Idiomas" : "lang",
    }
    miscdata = {}
    for tr in datatable.xpath(".//tr") :
        k = GetFirstStrip(tr.css("th.label::text").extract())
        #v = tr.css("td.data::text").extract()[0].strip()
        v = ' '.join(map(lambda e: e.strip(), tr.css("td.data").xpath(".//text()").extract())).strip()
        if kvtable.has_key(k) :
            itemdata[kvtable[k]] = v
        else :
            miscdata[k] = v
    itemdata["miscdata"] = json.dumps(miscdata)
    return itemdata

#isbn = "0-440-21145-X"
#QueryISBN(isbn)
