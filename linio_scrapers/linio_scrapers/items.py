# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class CuspideItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    #pass
    title = scrapy.Field()
    author = scrapy.Field()
    publisher = scrapy.Field()
    prices = scrapy.Field()
    miscdata = scrapy.Field()
    cat = scrapy.Field()
    desc = scrapy.Field()
    img = scrapy.Field()
    url = scrapy.Field()

class CuspideListingItem(scrapy.Item):
    title = scrapy.Field()
    author = scrapy.Field()
    prices = scrapy.Field()
    cat = scrapy.Field()
    resenia = scrapy.Field()
    img = scrapy.Field()
    url = scrapy.Field()

class GenericItem (scrapy.Item) :
    title = scrapy.Field()
    subtitle = scrapy.Field()
    author = scrapy.Field()
    publisher = scrapy.Field()
    prices = scrapy.Field()
    miscdata = scrapy.Field()
    cat = scrapy.Field()
    subject = scrapy.Field()
    desc = scrapy.Field()
    img = scrapy.Field()
    img_hd = scrapy.Field()
    url = scrapy.Field()
    lang = scrapy.Field()
    pages = scrapy.Field()
    year = scrapy.Field()
    pubdate = scrapy.Field()
    edition = scrapy.Field()
    availavility = scrapy.Field()
    format = scrapy.Field()
    isbn = scrapy.Field()
    dimensions = scrapy.Field()
    weight = scrapy.Field()
    extract = scrapy.Field()
    video_trailer = scrapy.Field()
    backcover_text = scrapy.Field()

class GenericListingItem(scrapy.Item):
    title = scrapy.Field()
    author = scrapy.Field()
    prices = scrapy.Field()
    cat = scrapy.Field()
    resenia = scrapy.Field()
    img = scrapy.Field()
    url = scrapy.Field()

class GenericCategoryListingItem(scrapy.Item):
    title = scrapy.Field()
    url = scrapy.Field()

