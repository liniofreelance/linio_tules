<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="panel panel-default"><div class="panel-body">
        <h1>Trabajos:</h1>
        <table class="table">
            <thead>
                <th>ID</th>
                <th>Nombre</th>
                <th>Hora de inicio</th>
                <th>Estado</th>
                <th colspan="2">Descargar</th>
                <th>Ver estado</th>
                <th>Cancelar</th>
                <th>Borrar</th>
            </thead>
            <tbody>
                {% for job in jobs %}
                <tr>
                    <td>{{ job._id }}</td>
                    <td>{{ job.name }}</td>
                    <td>{{ job.starttime }}</td>
                    <td>{{ job.status }}</td>
                    <td><a class="btn btn-default btn-success" href='/downloadjobresults?fmt=csv&id={{ job._id }}&jobtype={{ jobtype }}'>CSV</a></td>
                    <td><a class="btn btn-default btn-success" href='/downloadjobresults?fmt=xls&id={{ job._id }}&jobtype={{ jobtype }}'>XLS</a></td>
                    <td><a class="btn btn-default btn-success" href='/viewjob?id={{ job._id }}&jobtype={{ jobtype }}'>Ver</a></td>
                    <td><a class="btn btn-default btn-warning" href='/canceljob?id={{ job._id }}&jobtype={{ jobtype }}'>Cancelar</a></td>
                    <td><a class="btn btn-default btn-danger" href='/deletejob?id={{ job._id }}&jobtype={{ jobtype }}'>Borrar</a></td>
                </tr>
                {% endfor %}
            </tbody>
        </table>
        <a class="btn btn-default" href="/">Volver</a>
        </div></div>
    </body>
</html>
