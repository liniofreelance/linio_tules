#!/usr/bin/python
# -*- coding: utf-8 -*-

import json
import xlsxwriter    
import csv
import gc
import argparse
import os

import helpers

def convert_scrapy_json_to_all_formats (infn, outdir=None) :
    fn = os.path.basename(infn)
    basefn, ext = os.path.splitext(fn)
    if outdir is None :
        outdir = os.path.dirname(outdir)
    for fmt in ["xls", "csv"] :
        outfn = "%s/%s.%s" % (outdir, basefn, fmt)
        convert_scrapy_json (infn, outfn, fmt)
        
def convert_scrapy_json (infn, outfn, fmt) :
    fieldnames = collect_fieldnames_from_scrapy_json(infn)
    data = get_scrapy_json_data_reader(infn)
    return saveTo(fmt, outfn, fieldnames, data)

def collect_fieldnames_from_scrapy_json (infn) :
    keys = set()
    for r in get_scrapy_json_data_reader (infn) :
        keys |= set(r.keys())
    return list(keys)

def get_scrapy_json_data_reader (infn) :
    keys = set()
    inf = open(infn)
    for rawline in inf :
        #if row % 10000 == 0:
        #    print row
        rawline = rawline.strip()
        if rawline[0] == "[" :
            rawline = rawline[1:]
        if rawline[-1] in ",]" :
            rawline = rawline[:-1]
        yield json.loads(rawline)
    inf.close()

def saveTo(fmt, outfn, fieldnames, data):
    return available_formats[fmt]["formatter"](outfn, fieldnames, data)

def makeThisCleanUnicode (coso) :
    try :
        return str(coso)
    except UnicodeEncodeError:
        return coso
    return None

def normalizeValue(v) :
    if type(v) == dict :
        return ' '.join(["%s_%s" % (makeThisCleanUnicode(k2), makeThisCleanUnicode(v)) for (k2,v) in v.items()])
    if type(v) == list :
        return ' '.join(map(makeThisCleanUnicode, v))
    if v is None :
        return ''
    return v

def saveToCSV (outfn, fieldnames, data) :
    f = open(outfn, "wb")
    #writer = csv.DictWriter(f, fieldnames=fieldnames)
    writer = helpers.DictUnicodeWriter(f, fieldnames)
    #writer.fieldnames = fieldnames
    writer.writeheader()
    for record in data :
        r = { k : normalizeValue(v) for (k,v) in record.items()}
        writer.writerow(r)
    f.close()
    gc.collect()


def saveToXLSX (outfn, fieldnames, data) :
    workbook = xlsxwriter.Workbook(outfn, {'constant_memory': True, 'strings_to_urls': False})
    worksheet = workbook.add_worksheet()
    
    row = 0
    col = 0

    for col,k in enumerate(fieldnames) :
        worksheet.write(row, col, k)
    #pprint(data)
    for row, record in enumerate(data) :
        row +=1
        for col,k in enumerate(fieldnames) :
            v = normalizeValue(record.get(k,""))
            ret = worksheet.write(row, col, v)
    
    workbook.close()
    gc.collect()

available_formats = {
    "csv" : {
        "formatter" : saveToCSV,
    },
    "xlsx" : {
        "formatter" : saveToXLSX,
    },
    "xls" : {
        "formatter" : saveToXLSX,
    },
}

if __name__ == "__main__" :
    parser = argparse.ArgumentParser(description='Convert scrapy json output to other formats (by default all available)')
    parser.add_argument('--infn', action="store", required=True)
    parser.add_argument('--outdir', action="store", default='./')
    args = parser.parse_args()
    
    convert_scrapy_json_to_all_formats (args.infn.strip(), args.outdir.strip())