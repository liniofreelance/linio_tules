# -*- coding: utf-8 -*-
"""
Data extraction implementation module.

Sample API calls:
https://api.mercadolibre.com/categories/MLA48887
https://api.mercadolibre.com/sites/MLA/categories
https://api.mercadolibre.com/sites/MLA/search/#options
"""

from __future__ import print_function  # -*- coding: utf-8 -*-

import datetime
import json
import os

import dateutil.parser
import requests
import requests_cache
from openpyxl import load_workbook
import xlsxwriter

MAX_ITEMS = 50
DEFAULT_NICKNAME = "UNKNOWN"


requests_cache.install_cache("simple_cache", expire_after=60 * 30)
# To clear the cache use this:
# requests_cache.clear()


def getCategoryListing(catcode, max_items=MAX_ITEMS, adult_content=True):
    if adult_content:
        adult_content = "yes"
    else:
        adult_content = "no"
    caturltpl = "https://api.mercadolibre.com/sites/MLA/search?category=%s&condition=new&power_seller=yes&limit=%s&offset=%s&adult_content=%s"  # pylint: disable=C0301
    limit = min(max_items, MAX_ITEMS)
    offset = 0
    resdata = []
    while True:
        caturl = caturltpl % (catcode, limit, offset, adult_content)
        print(caturl)
        rqst = requests.get(caturl)
        rqstdata = rqst.json()
        if "error" in rqstdata:
            print("Error getting category %s" % catcode)
            continue
        for r in rqstdata.get("results", []):
            itemdata = {
                "id": r["id"],
                "permalink": r["permalink"],
                "title": unicode(r["title"]),
                "price": r["price"],
                "seller_id": str(r["seller"]["id"]),
                "sales": r.get("sold_quantity", 0),
                "stock": r.get("available_quantity", 0),
                "free_shiping": r["shipping"].get("free_shipping", False),
                "cat_id": str(r["category_id"].decode("utf8")),
                "match": str(r["id"].decode("utf8")),
            }
            resdata += [itemdata]
        if (offset + len(rqstdata["results"])) >= min(
            max_items, rqstdata["paging"]["total"]
        ):
            break
        offset += len(rqstdata["results"])
    return resdata


def getSellerNick(userid):
    usersurltpl = "https://api.mercadolibre.com/users/%s"
    usersurl = usersurltpl % userid
    rqst = requests.get(usersurl)
    nickname = rqst.json().get("nickname", DEFAULT_NICKNAME)
    # print(nickname)
    return nickname


def getItemsData(itemids, chunk_size=20):
    itemsurltpl = "https://api.mercadolibre.com/items?ids=%s"
    res = []
    chunks = [itemids[x : x + chunk_size] for x in range(0, len(itemids), chunk_size)]
    for itemidchunk in chunks:
        itemsurl = itemsurltpl % ",".join(itemidchunk)
        # print(itemsurl)
        rqst = requests.get(itemsurl)
        r = rqst.json()
        if rqst.status_code == 400:
            raise ValueError(r['message'])
        for item_r in r:
            item_data = item_r["body"]
            if item_data["status"] == 404:
                print("Item not found %s" % item_data["id"])
                continue
            resitem = {
                "id": unicode(item_data["id"]),
                "since": dateutil.parser.parse(item_data["start_time"]).strftime(
                    "%d/%m/%Y"
                ),
                "days": (
                    datetime.date.today()
                    - dateutil.parser.parse(item_data["start_time"]).date()
                    + datetime.timedelta(days=1)
                ).days,
                "parent": item_data.get("parent_item_id", None),
                "sold_quantity": item_data.get("sold_quantity", 0),
                "stock_0": item_data.get("initial_quantity", 0),
            }
            if resitem["days"] < 7:
                resitem["!"] = 1
            if 7 <= resitem["days"] <= 14:
                resitem["!"] = 2
            if resitem["days"] > 14:
                resitem["!"] = 3
            res += [resitem]
    return res


catcache = {}


def getCategoryData(catcode, use_cache=False):
    """Get data of the given catcode.

    Test calls:
        getCategoryData ("MLA48887")
        getCategoryData ("MLA1246")
        getCategoryData ("MLA6556")
        getCategoryData ("MLA6566")
    """
    if use_cache:
        if catcode in catcache:
            return catcache[catcode]

    catsurltpl = "https://api.mercadolibre.com/categories/%s"
    catsurl = catsurltpl % catcode
    r = requests.get(catsurl).json()
    catpath = r["path_from_root"]

    if use_cache:
        for i, cp in enumerate(catpath):
            catcache[cp["id"]] = catpath[: i + 1]
    return catpath


def getCategoryItems(catcode, max_items):
    """Get up to `max_items` from category `catcode`."""
    catitems = getCategoryListing(catcode, max_items=max_items)
    if not catitems:
        return []

    print("Getting subcategories from catcode: %s" % catcode)
    # Add category data: sub1, sub2, sub3
    for citem in catitems:
        cats = getCategoryData(citem["cat_id"])
        # print(catcode, cats)
        for j, cat in enumerate(cats):
            if cat["id"] == catcode:
                cats = cats[j + 1 :][-3:]
                break
        for j, cat in enumerate(cats):
            citem["sub%d" % (j + 1)] = cat["name"]

    # Add full item data:
    print("Getting extra data")
    catitems_ids = [item["id"] for item in catitems]
    extradata_items = getItemsData(catitems_ids)
    for citem in catitems:
        for eitem in extradata_items:
            if citem["id"] == eitem["id"]:
                citem.update(eitem)

    # Add seller nick
    print("Getting seller nicknames")
    for citem in catitems:
        citem["seller_nick"] = getSellerNick(citem["seller_id"])

    # Add sameday shiping data: matcheo (TODO)

    # Add parent data:
    # sales_0 = parentitem["sold_quantity"]
    print("Getting parent item data")
    # item_parents_ids = [item["parent"] for item in catitems if item["parent"] != None]
    item_parents_ids = [
        item["parent"]
        for item in catitems
        if (("parent" in item) and (item["parent"] is not None))
    ]
    item_parents = getItemsData(item_parents_ids)
    for citems in catitems:
        for pitem in item_parents:
            if "parent" in citems:
                if citems["parent"] == pitem["id"]:
                    citems["sales_0"] = pitem["sold_quantity"]

    print("Calculating remaining fields")
    for citems in catitems:
        sales_ok = 0
        sales = citem.get("sales", "")
        sales_0 = citem.get("sales_0", "")
        stock = citem["stock"]
        stock_0 = citem["stock_0"]
        if (sales_0 != "") & (sales >= sales_0) & (stock_0 >= stock):
            sales_ok = sales - sales_0
        else:
            if sales < sales_0:
                sales_ok = stock_0 - stock
            elif stock < stock_0:
                sales_ok = sales - sales_0

        citem["sales_ok"] = sales_ok
        citem["avg_q"] = citem["sales_ok"] / float(citem.get("days", 1))
        # citem["avg_$"] = round(citem["price"] / float(citem.get('days',1)), 2)
        citem["avg_$"] = citem["avg_q"] * citem["price"]

    return catitems


DEFAULT_COL_TITLE_MAP = [
    ("id", "ID"),
    ("permalink", "url"),
    ("title", "Nombre del Producto"),
    ("price", "Precio"),
    ("seller_nick", "Nickname"),
    ("seller_id", "Seller"),
    ("sales", "Ventas"),
    ("stock", "Stock"),
    ("stock_0", "Stock 0"),
    ("free_shiping", "Free Shipping"),
    ("sameday", "Sameday Match"),
    ("parent", "Parent"),
    ("since", "Desde"),
    ("sales_0", "Ventas 0"),
    ("sales_ok", "Ventas OK"),
    ("days", "Días"),
    ("avg_q", "PromedioQ"),
    ("avg_$", "Promedio$"),
    ("ranking_q", "Ranking Q"),
    ("ranking_$", "Ranking $"),
    ("!", "!"),
    ("cat_id", "cat_id"),
    ("sub1", "sub1"),
    ("sub2", "sub2"),
    ("sub3", "sub3"),
]

CONSOLIDATED_COLTITLES = [("category", "Categoría")] + DEFAULT_COL_TITLE_MAP


def formatRows(items, coltitles):
    outbuf = [[t for (k, t) in coltitles]]
    for item in items:
        outbuf += [[item.get(k, "") for (k, t) in coltitles]]
    return outbuf


def load_cat_code_names_lookup(fn=None):
    if fn is None:
        fn = os.path.dirname(os.path.abspath(__file__)) + "/catsmini.json"
    catsf = open(fn)
    catnamesncodes_lookup = {k: v["name"] for (k, v) in json.load(catsf).items()}
    catsf.close()
    return catnamesncodes_lookup


def getCategoryFile(outfn, target_catnamesncodes, max_items=100):
    # outfn = 'Informacion por Categorias %s.xlsx' % datetime.datetime.today().strftime("%d-%m-%Y")
    allcatsdata = {}
    for (catname, catcode) in target_catnamesncodes:
        print("Getting category: %s" % catcode)
        allcatsdata[catcode] = getCategoryItems(catcode, max_items)
        print("Finished getting category: %s" % catcode)

    workbook = xlsxwriter.Workbook(
        outfn, {"constant_memory": True, "strings_to_urls": False}
    )
    for (catname, catcode) in target_catnamesncodes:
        # print("Getting category: %s" % catcode)
        # catdata = getCategoryItems (catcode, max_items)
        # print("Finished getting category: %s" % catcode)
        catdata = allcatsdata[catcode]
        fmtdcatdata = formatRows(catdata, DEFAULT_COL_TITLE_MAP)
        # pprint(fmtdcatdata)
        # sheetname = catname.encode("utf8")
        sheetname = catname
        ws = workbook.add_worksheet(sheetname)
        for (i, row) in enumerate(fmtdcatdata):
            for (j, celldata) in enumerate(row):
                # celldata = set(celldata)
                if isinstance(celldata, str):
                    celldata = celldata.decode("utf8")
                # if type(celldata) == int :
                #    celldata = str(celldata)
                _ = ws.write(i, j, celldata)

    workbook.close()


def consolidateResults(infn, outfn):
    outworkbook = xlsxwriter.Workbook(
        outfn, {"constant_memory": True, "strings_to_urls": False}
    )
    outws = outworkbook.add_worksheet("Consolidados Total")
    wb = load_workbook(filename=infn, read_only=False)

    allrows_i = 0
    for (j, celldata) in enumerate([t for (k, t) in CONSOLIDATED_COLTITLES]):
        if isinstance(celldata, str):
            celldata = celldata.decode("utf8")
        _ = outws.write(allrows_i, j, celldata)

    allrows_i = 1
    for sn in wb.sheetnames:
        ws = wb.get_sheet_by_name(sn)
        # print("#"*80)
        # print(ws.title)
        rowiter = iter(ws.rows)
        _ = next(rowiter)
        for row in rowiter:
            rowdata = [None] * len(CONSOLIDATED_COLTITLES)
            rowdata[0] = sn
            for cell in row:
                if cell.value is not None:
                    rowdata[ord(cell.column) - ord("A") + 1] = cell.value
            for (j, celldata) in enumerate(rowdata):
                if isinstance(celldata, str):
                    celldata = celldata.decode("utf8")
                # if isinstance(celldata, int):
                #    celldata = str(celldata)
                _ = outws.write(allrows_i, j, celldata)
            allrows_i += 1

    outworkbook.close()
