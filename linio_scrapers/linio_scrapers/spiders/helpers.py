
from scrapy.selector import SelectorList

def GetFirst(l,v=None) :
    if type (l) == SelectorList :
        l = l.extract()
    if len(l)>=1 :
        v = l[0]
    if type(v) == str :
        v = v.strip()
    return v
